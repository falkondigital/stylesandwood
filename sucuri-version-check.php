<?php

@set_time_limit(0);
@ini_set("max_execution_time",0);
@set_time_limit(0);
@ignore_user_abort(TRUE);

$myversion='20160217_1825'; //Version to be updated on every build

$HTACCESS = ".htaccess";
$WPVERSION = "version.php";
$OSVERSION = "application_top.php";

$vulnerable_plugins = array(
    'fancybox.php' => array( '3.0.2', 'https://blog.sucuri.net/2015/02/zero-day-in-the-fancybox-for-wordpress-plugin.html', 'Fancybox' ),
    'gravityforms.php' => array( '1.8.20', 'https://blog.sucuri.net/2015/02/malware-cleanup-to-arbitrary-file-upload-in-gravity-forms.html', 'Gravity Forms' ),
    'hdflvvideoshare.php' => array( '2.8', '', 'HDFLV Videoshare' ),
);

$plugins = array_keys( $vulnerable_plugins );
$plugins = '*,*' . implode ( '*,*', $plugins ) . '*,*';

/* If running via terminal. */
if(!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['SHELL']))
{
    parse_str(implode('&', array_slice($argv, 1)), $_GET);
}


if(!isset($_GET['srun']))
{
    @unlink("sucuri-cleanup.php");
    @unlink("sucuri-version-check.php");
    @unlink("sucuri-wpdb-clean.php");
    @unlink("sucuri-db-cleanup.php");
    @unlink("sucuri_db_clean.php");
    @unlink("sucuri_listcleaned.php");
    @unlink("sucuri-filemanager.php");
    @unlink(__FILE__);
    exit(0);
}
//Added support for PHP 4.4.9 https://sucuri.atlassian.net/browse/RESEARCH-1720
  if(!function_exists('file_put_contents')) {
    function file_put_contents($filename, $data, $file_append = false) {
      $fp = fopen($filename, (!$file_append ? 'w+' : 'a+'));
        if(!$fp) {
          trigger_error('file_put_contents cannot write in file.', E_USER_ERROR);
          return;
        }
      fputs($fp, $data);
      fclose($fp);
    }
  }

$versions = '{"signature":"sucuri-current-versions","joomla":["3.4.8"],"wordpress":["4.4.2"],"drupal":["6.37","7.39"],"magento":["1.9.2.2"],"jce":["2.3.3.2","2.3.4.4"],"phpbb":["3.1.5"],"vbulletin":["4.2.2","3.8.7 Patch Level 3"],"jetpack":["3.6"],"zenphoto":["1.4.9"],"modX":{"evolution":"1.0.15","revolution":"2.4.2-pl"}}'; //Update this line with the most up-to-date versions from refresh-versions.php - update on every build

$versions = json_dcode( $versions );

define('SOFTWARE', '{\"wordpress\":{\"1.2\":[\"WordPress 1.2-1.2.1 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 1.2 - HTTP Response Splitting Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC Eval Injection  Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Email Spoofing Found in current wordpress version.\"],\"1.2.1\":[\"WordPress 1.2-1.2.1 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC Eval Injection  Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Email Spoofing Found in current wordpress version.\"],\"1.5\":[\"WordPress 1.5 wp-trackback.php tb_id Parameter SQL Injection Found in current wordpress version.\",\"WordPress &lt;= 1.5 Multiple Vulnerabilities (XSS, SQLi) Found in current wordpress version.\",\"WordPress 1.5 template-functions-post.php Multiple Field XSS Found in current wordpress version.\",\"WordPress 1.5 &amp; 1.5.1.1 - SQL Injection Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC Eval Injection  Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Email Spoofing Found in current wordpress version.\"],\"1.5.1\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC Eval Injection  Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Email Spoofing Found in current wordpress version.\"],\"1.5.1.1\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.1 &quot;add new admin&quot; SQL Injection Exploit Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.1 SQL Injection Exploit Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 1.5 &amp; 1.5.1.1 - SQL Injection Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC Eval Injection  Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Email Spoofing Found in current wordpress version.\"],\"1.5.1.2\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC SQL Injection Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - XMLRPC Eval Injection  Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Multiple Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 1.5.1.2 - Email Spoofing Found in current wordpress version.\"],\"1.5.1.3\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"Wordpress &lt;= 1.5.1.3 Remote Code Execution eXploit (metasploit) Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"1.5.2\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.1\":[\"Wordpress 1.5.1 - 2.0.2 wp-register.php Multiple Parameter XSS Found in current wordpress version.\",\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.2\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress &lt;= 2.0.2 (cache) Remote Shell Injection Exploit Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0.2 - 2.0.4 Paged Parameter SQL Injection Vulnerability Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.3\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0.2 - 2.0.4 Paged Parameter SQL Injection Vulnerability Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.4\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0.2 - 2.0.4 Paged Parameter SQL Injection Vulnerability Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.5\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"Wordpress 2.0.5 Trackback UTF-7 Remote SQL Injection Exploit Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.6\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"Wordpress &lt;= 2.0.6 wp-trackback.php Remote SQL Injection Exploit Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.7\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.8\":[\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.9\":[\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.10\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.0.11\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.1\":[\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.1.1\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.1.1 - Command Execution Backdoor Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 2.1.1 - RCE Backdoor Found in current wordpress version.\"],\"2.1.2\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress &#039;year&#039; Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 2.1.2 Authenticated XMLRPC SQL Injection Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.1.3\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"Wordpress 2.1.3 admin-ajax.php SQL Injection Blind Fishing Exploit Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.2\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.2 (wp-app.php) Arbitrary File Upload Exploit Found in current wordpress version.\",\"Wordpress 2.2 (xmlrpc.php) Remote SQL Injection Exploit Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.2.1\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.2.2\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.2.3\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.3\":[\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.3.1\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"Wordpress &lt;= 2.3.1 Charset Remote SQL Injection Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.3.2\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.3.3\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.5\":[\"Wordpress 2.5 Cookie Integrity Protection Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.5.1\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.6\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.6.1\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"Wordpress 2.6.1 (SQL Column Truncation) Admin Takeover Exploit Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.6.2\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.6.3\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.6.5\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.7\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.7.1\":[\"WordPress 2.0 - 2.7.1 admin.php Module Configuration Security Bypass Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8.1\":[\"Wordpress 2.8.1 (url) Remote Cross Site Scripting Exploit Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8.2\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8.3\":[\"Wordpress &lt;= 2.8.3 Remote Admin Reset Password Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8.4\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8.5\":[\"WordPress &lt;= 2.8.5 Unrestricted File Upload Arbitrary PHP Code Execution Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.8.6\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.9\":[\"WordPress 2.9 Failure to Restrict URL Access Found in current wordpress version.\",\"WordPress 2.9 - Failure to Restrict URL Access Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.9.1\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"2.9.2\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\"],\"3.0\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.0.5 wp-admin\/press-this.php Privilege Escalation Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0 Remote Authenticated Administrator Add Action Bypass Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.0.1\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.0.5 wp-admin\/press-this.php Privilege Escalation Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/comment.php Bypass Spam Restrictions Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Multiple Cross-Site Scripting (XSS) in request_filesystem_credentials() Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 Cross-Site Scripting (XSS) in wp-admin\/plugins.php Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 wp-includes\/capabilities.php Remote Authenticated Administrator Delete Action Bypass Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0 - 3.0.1 SQL Injection in do_trackbacks() Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.0.2\":[\"WordPress XML-RPC Interface Access Restriction Bypass Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.0.5 wp-admin\/press-this.php Privilege Escalation Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.0.3\":[\"WordPress 2.0 - 3.0.1 SQL Injection in do_trackbacks() Found in current wordpress version.\",\"Wordpress 3.0.3 stored XSS IE7,6 NS8.1 Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.0.5 wp-admin\/press-this.php Privilege Escalation Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.0.4\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.0.5 wp-admin\/press-this.php Privilege Escalation Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.0.5\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.0.5 wp-admin\/press-this.php Privilege Escalation Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.0.6\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.1\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 3.1 PCRE Library Remote DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.1.1\":[\"WordPress 3.1 PCRE Library Remote DoS Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.1.2\":[\"Wordpress &lt;= 3.1.2 Clickjacking Vulnerability Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.1.3\":[\"WordPress 3.1.3 wp-admin\/link-manager.php Multiple Parameter SQL Injection Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.1.4\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.2\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.2.1\":[\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.3\":[\"WordPress 3.3 Reflected Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.3.1\":[\"WordPress 3.3.1 Multiple vulnerabilities including XSS and Privilege Escalation Found in current wordpress version.\",\"Wordpress 3.3.1 - Multiple CSRF Vulnerabilities Found in current wordpress version.\",\"WordPress 2.5 - 3.3.1 XSS in swfupload Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.3.2\":[\"WordPress 3.3.2 - 3.5 Cross-Site Scripting (XSS) (Issue 3) Found in current wordpress version.\",\"Wordpress 3.3.1 Multiple CSRF Vulnerabilities Found in current wordpress version.\",\"WordPress 3.3.2 Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 Cross-Site Scripting (XSS) in wp-includes\/default-filters.php Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/media-upload.php sensitive information disclosure or bypass Found in current wordpress version.\",\"WordPress &lt;= 3.3.2 wp-admin\/includes\/class-wp-posts-list-table.php sensitive information disclosure by visiting a draft Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.3.3\":[\"WordPress 3.3.2 - 3.5 Cross-Site Scripting (XSS) (Issue 3) Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.4\":[\"Wordpress 3.4 - 3.5.1 \/wp-admin\/users.php Malformed s Parameter Path Disclosure Found in current wordpress version.\",\"WordPress 3.4 - 3.5.1 DoS in class-phpass.php Found in current wordpress version.\",\"WordPress 3.3.2 - 3.5 Cross-Site Scripting (XSS) (Issue 3) Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.4.1\":[\"Wordpress 3.4 - 3.5.1 \/wp-admin\/users.php Malformed s Parameter Path Disclosure Found in current wordpress version.\",\"WordPress 3.4 - 3.5.1 DoS in class-phpass.php Found in current wordpress version.\",\"WordPress 3.3.2 - 3.5 Cross-Site Scripting (XSS) (Issue 3) Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.4.2\":[\"Wordpress 3.4 - 3.5.1 \/wp-admin\/users.php Malformed s Parameter Path Disclosure Found in current wordpress version.\",\"WordPress 3.4 - 3.5.1 DoS in class-phpass.php Found in current wordpress version.\",\"WordPress 3.3.2 - 3.5 Cross-Site Scripting (XSS) (Issue 3) Found in current wordpress version.\",\"WordPress 3.4.2 Cross Site Request Forgery Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.5\":[\"Wordpress 3.4 - 3.5.1 \/wp-admin\/users.php Malformed s Parameter Path Disclosure Found in current wordpress version.\",\"WordPress 3.4 - 3.5.1 DoS in class-phpass.php Found in current wordpress version.\",\"WordPress 3.3.2 - 3.5 Cross-Site Scripting (XSS) (Issue 3) Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC Pingback API Internal\/External Port Scanning Found in current wordpress version.\",\"WordPress 1.5.1 - 3.5 XMLRPC pingback additional issues Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 3.5 Shortcodes \/ Post Content Multiple Unspecified XSS Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.5-3.5.1 oEmbed Unspecified XML External Entity (XXE) Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.5.1\":[\"Wordpress 3.4 - 3.5.1 \/wp-admin\/users.php Malformed s Parameter Path Disclosure Found in current wordpress version.\",\"WordPress 3.4-3.5.1 DoS in class-phpass.php Found in current wordpress version.\",\"WordPress 3.5.1 Multiple XSS Found in current wordpress version.\",\"WordPress 3.5.1 TinyMCE Plugin Flash Applet Unspecified Spoofing Weakness Found in current wordpress version.\",\"WordPress File Upload Unspecified Path Disclosure Found in current wordpress version.\",\"WordPress 3.5-3.5.1 oEmbed Unspecified XML External Entity (XXE) Found in current wordpress version.\",\"WordPress 3.5-3.5.1 Multiple Role Remote Privilege Escalation Found in current wordpress version.\",\"WordPress 3.5-3.5.1 HTTP API Unspecified Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.5.2\":[\"WordPress 3.5.2 Media Library Multiple Function Path Disclosure Found in current wordpress version.\",\"WordPress 3.5.2 SWFUpload Content Spoofing Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.6\":[\"WordPress 3.6 PHP Object Injection Found in current wordpress version.\",\"WordPress 3.6 SWF\/EXE File Upload XSS Weakness Found in current wordpress version.\",\"WordPress 3.0 - 3.6 Crafted String URL Redirect Restriction Bypass Found in current wordpress version.\",\"WordPress 3.6 Post Authorship Spoofing Found in current wordpress version.\",\"WordPress 3.6 HTML File Upload XSS Weakness Found in current wordpress version.\",\"WordPress 3.6 Multiple Function Path Disclosure Found in current wordpress version.\",\"WordPress 3.6 Multiple Script Arbitrary Site Redirect Found in current wordpress version.\",\"WordPress 3.6 _wp_http_referer Parameter Reflected XSS Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.6.1\":[\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\"],\"3.7\":[\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.1\":[\"WordPress 3.7.1 &amp; 3.8.1 Potential Authentication Cookie Forgery Found in current wordpress version.\",\"WordPress 3.7.1 &amp; 3.8.1 Privilege escalation: contributors publishing posts Found in current wordpress version.\",\"WordPress 3.7.1 &amp; 3.8 - Cleartext Admin Credentials Disclosure Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.2\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.3\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.4\":[\"WordPress &lt;= 4.0 - CSRF in wp-login.php Password Reset Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.5\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.6\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.7\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.8\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.9\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.10\":[\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.11\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.7.12\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8\":[\"WordPress 3.7.1 &amp; 3.8 - Cleartext Admin Credentials Disclosure Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.1\":[\"WordPress 1.0 - 3.8.1 administrator exploitable blind SQLi Found in current wordpress version.\",\"WordPress 3.7.1 &amp; 3.8.1 Potential Authentication Cookie Forgery Found in current wordpress version.\",\"WordPress 3.7.1 &amp; 3.8.1 Privilege escalation: contributors publishing posts Found in current wordpress version.\",\"WordPress Plupload Unspecified XSS Found in current wordpress version.\",\"WordPress 3.5 - 3.7.1 XML-RPC DoS Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.2\":[\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.3\":[\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.4\":[\"WordPress &lt;= 4.0 - CSRF in wp-login.php Password Reset Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.5\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.6\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.7\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.8\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.9\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.10\":[\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.11\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.8.12\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9\":[\" WordPress 3.9 &amp; 3.9.1 Unlikely Code Execution Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.9, 3.9.1, 3.9.2, 4.0 - XSS in Media Playlists Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.9-4.1.1 - Same-Origin Method Execution Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.1\":[\" WordPress 3.9 &amp; 3.9.1 Unlikely Code Execution Found in current wordpress version.\",\"WordPress 2.0.3 - 3.9.1 (except 3.7.4 \/ 3.8.4) CSRF Token Brute Forcing Found in current wordpress version.\",\"WordPress 3.0 - 3.9.1 Authenticated Cross-Site Scripting (XSS) in Multisite Found in current wordpress version.\",\"WordPress 3.6 - 3.9.1 XXE in GetID3 Library Found in current wordpress version.\",\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.9, 3.9.1, 3.9.2, 4.0 - XSS in Media Playlists Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.2\":[\"WordPress 3.4.2 - 3.9.2 Does Not Invalidate Sessions Upon Logout Found in current wordpress version.\",\"WordPress 3.0-3.9.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.9, 3.9.1, 3.9.2, 4.0 - XSS in Media Playlists Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - CSRF in wp-login.php Password Reset Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.3\":[\"WordPress &lt;= 4.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.4\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.5\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.6\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.7\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.8\":[\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.9\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"3.9.10\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0\":[\"WordPress &lt;= 4.0 - Long Password Denial of Service (DoS) Found in current wordpress version.\",\"WordPress &lt;= 4.0 - Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.9, 3.9.1, 3.9.2, 4.0 - XSS in Media Playlists Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.9-4.1.1 - Same-Origin Method Execution Found in current wordpress version.\",\"WordPress &lt;= 4.0 - CSRF in wp-login.php Password Reset Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.1\":[\"WordPress 3.9-4.1.1 - Same-Origin Method Execution Found in current wordpress version.\",\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.2\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.3\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.4\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.5\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.6\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.7\":[\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.8\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.0.9\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1\":[\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.9-4.1.1 - Same-Origin Method Execution Found in current wordpress version.\",\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 4.1 - 4.1.1 - Arbitrary File Upload Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.1\":[\"WordPress &lt;= 4.1.1 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.9-4.1.1 - Same-Origin Method Execution Found in current wordpress version.\",\"WordPress &lt;= 4.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 4.1 - 4.1.1 - Arbitrary File Upload Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.2\":[\"WordPress &lt;= 4.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.3\":[\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.4\":[\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.5\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.6\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.7\":[\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.8\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.1.9\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2\":[\"WordPress &lt;= 4.2 - Unauthenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2.1\":[\"WordPress 4.1-4.2.1 - Genericons Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2.2\":[\"WordPress &lt;= 4.2.2 - Authenticated Stored Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2.3\":[\"WordPress &lt;= 4.2.3 - wp_untrash_post_comments SQL Injection  Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Timing Side Channel Attack Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Widgets Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Nav Menu Title Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.2.3 - Legacy Theme Preview Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2.4\":[\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2.5\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.2.6\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.3\":[\"WordPress &lt;= 4.3 - Authenticated Shortcode Tags Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - User List Table Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress &lt;= 4.3 - Publish Post and Mark as Sticky Permission Issue Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.3.1\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.3.2\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.4\":[\"WordPress  3.7-4.4 - Authenticated Cross-Site Scripting (XSS) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"],\"4.4.1\":[\"WordPress 3.7-4.4.1 - Local URIs Server Side Request Forgery (SSRF) Found in current wordpress version.\",\"WordPress 3.7-4.4.1 - Open Redirect Found in current wordpress version.\"]}}');

$software = stripslashes( SOFTWARE );
$software = json_dcode( $software, true );

if ( $versions == NULL || $software == NULL )
{
    echo "ERROR: Unable to get current versions. Please contact support@sucuri.net for guidance.\n";
    exit( 1 );
}

//json_dcode implementation which works with small jsons
function json_dcode($json, $assoc = false) {
    $match = '/".*?(?<!\\\\)"/';

    $string = preg_replace( $match, '', $json );
    $string = preg_replace( '/[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/', '', $string );

    if ( $string != '' )
    {
        return null;
    }

    $s2m = array();
    $m2s = array();

    preg_match_all( $match, $json, $m );

    foreach ( $m[0] as $s )
    {
        $hash = '"' . md5( $s ) . '"';
        $s2m[$s] = $hash;
        $m2s[$hash] = str_replace( '$', '\$', $s );
    }

    $json = strtr( $json, $s2m );

    $a = ( $assoc ) ? '' : '( object ) ';

    $data = array(
        ':' => '=>',
        '[' => 'array(',
        '{' => "{$a}array(",
        ']' => ')',
        '}' => ')'
    );

    $json = strtr( $json, $data );

    $json = preg_replace( '~([\s\(,>])(-?)0~', '$1$2', $json );

    $json = strtr( $json, $m2s );

    $function = @create_function( '', "return {$json};" );
    $return = ( $function ) ? $function() : null;

    unset( $s2m );
    unset( $m2s );
    unset( $function );

    return $return;
}

function check_is_updated( $version, $latest_versions )
{
    $version = standardize_version( $version );
    
    if ( is_array( $latest_versions ) )
    {
         $lv = $latest_versions[0];
    }
    else
    {
         $lv = $latest_versions;
    }

    $latest = 1 < count( $latest_versions )
        ? find_most_similar( $version, $latest_versions )
        : standardize_version( $lv );


        if ( version_compare( $version, $latest ) >= 0 )
        {
            return TRUE;
        }

    return FALSE;
}

// finds the most similar version using XOR bitwise operation
// (same chars at the beginning of the string result in lower values of the result,
// lowest result gives the most similar from the beginning)
function find_most_similar( $version, $versions )
{
    $most_similar = FALSE;
    $most_similar_value = 0;

    foreach ( $versions as $current_version )
    {
        $end_result = standardize_version( $current_version );
        $end_result = str_pad( $end_result, strlen( $version ), '0' );
        $end_result = unpack( 'H*', $version ^ $end_result );
        $end_result = base_convert($end_result[1], 16, 10);

        if ( !$most_similar || $end_result < $most_similar_value )
        {
            $most_similar = $current_version;
            $most_similar_value = $end_result;
        }
    }

    return $most_similar;
}

function standardize_version( $version )
{
    $version = preg_replace( '/ Patch Level (\d+)/i', 'pl$1', $version );

    return $version;
}

function check_plugin( $dir, $file )
{
    global $vulnerable_plugins;

    $vulnerable_plugin = $vulnerable_plugins[$file];
    $safe_plugin_version = $vulnerable_plugin[0];
    $additional_info = FALSE;
    $plugin_name = $vulnerable_plugin[2];

    if ( array_key_exists( 1, $vulnerable_plugin ) )
    {
        $additional_info = $vulnerable_plugin[1];
    }

    $plugin_version = get_wp_plugin_version( "$dir/$file" );

    if ( $plugin_version && !check_is_updated( $plugin_version, $safe_plugin_version ) )
    {
        echo "Warning: vulnerable WordPress Plugin $plugin_name found at $dir/$file (version: $plugin_version) . Please update this plugin immediately.\n";

        if ( $additional_info )
        {
            echo "Additional info: $additional_info\n";
        }
    }
}

function get_wp_plugin_version( $file )
{
    $version = getfile( $file, "Version:" );

    $version = explode( ':', $version );

    if ( isset( $version[1] ) )
    {
        return trim( $version[1] );
    }

    return FALSE;
}

function getfile($file, $content)
{
    $fh = fopen($file, "r");
    if(!$fh)
    {
        echo "UNABLE TO CHECK\n";
        return(0);
    }
    while (($buffer = fgets($fh, 4096)) !== false)
    {
        if(strpos($buffer, $content) !== FALSE)
        {
            fclose($fh);
            return($buffer);
        }
    }
    fclose($fh);
    return(NULL);
}

function scanallfiles($dir)
{
    global $OSVERSION, $HTACCESS, $WPVERSION, $pluginfolders, $versions, $plugins, $software;
    $dh = opendir($dir);
    if(!$dh)
    {
        return(0);
    }

    if($dir == "./")
    {
        $dir = ".";
    }


    while (($myfile = @readdir($dh)) !== false)
    {
        if($myfile == "." || $myfile == "..")
        {
            continue;
        }

        if(strpos($myfile, "sucuribackup.") !== FALSE)
        {
            continue;
        }

        if(is_link($dir."/".$myfile))
        {
            echo "Skipping symlink directory: $dir/$myfile\n";
            continue;
        }

        if($myfile == $HTACCESS)
        {
            if(isset($_GET['ht']))
            {
                echo "Checking htaccess: ".$dir."/".$myfile;
            }
        }
        else if($myfile == "system.module" || $myfile == "bootstrap.inc")
        {
            $version = getfile($dir."/".$myfile, "define('VERSION'");

            if ( !$version )
            {
                continue;
            }

            $version = trim($version);
            $version = preg_replace("@^define\(\s*['\"]VERSION['\"]\s*,\s*['\"]([\d\.]+)['\"]\s*\);$@", '$1', $version);
            $realdir = dirname(dirname($dir));

            if($realdir == ".")
            {
                $realdir = "/ (main folder)";
            }

            if ( check_is_updated( $version, $versions->drupal ) )
            {
                echo "OK: Drupal install found (updated) inside: ".$realdir. " - Version: $version (from $dir/$myfile).\n";
            }
            else
            {
                echo "Warning: Found outdated Drupal install inside: ".$realdir. " - Version: $version (from $dir/$myfile) - Please update asap.\n";
            }
        }
        else if ( $myfile == "Mage.php" && false !== strpos( $dir, 'app' ) )
        {
            $config = @ file_get_contents("$dir/$myfile");
            if ( false === $config )
            {
                 continue;
            }

            $version = sprintf(
               "%s.%s.%s.%s", getOption( 'major', $config ),
                              getOption( 'minor', $config ),
                              getOption( 'revision', $config ),
                              getOption( 'patch',  $config )     
            );
            
            if ( check_is_updated( $version, $versions->magento ) )
            {
                echo "OK: Magento install found (updated) inside: ".$dir."/".$myfile." - Version: $version \n";
            }
            else
            {
                echo "Warning: Found outdated Magento install inside: ".$dir."/".$myfile." - Version: $version - Please update asap.\n";
            }
        }
        else if ( $myfile == "version.inc.php" )
        {
            $config = @ file_get_contents( "$dir/$myfile" );
            if ( $config === false )
            {
                 continue;
            }

            $version = getOption( 'modx_version', $config );
            if ( $version !== null )
            {
                 if ( check_is_updated( $version, $versions->modX->evolution ) )
                 {
                      echo "OK: ModX Evolution install found (updated) inside: $dir/$myfile - Version: $version \n";
                 }
                 else
                 {
                      echo "Warning: Found outdated ModX Evolution install inside: $dir/$myfile - Version: $version \n";
                 }
            }
            else
            {
                if ( preg_match_all( '@\$v\[\'([\S]+)\'\]\s*=\s*[\'](.*)[\'];@', $config, $m ) )
                {
                     if ( ! isset( $m[2][0],
                                   $m[2][1],
                                   $m[2][2],   
                                   $m[2][3] ) )
                     {
                          continue;
                     }
                     
                     $version = sprintf( '%s.%s.%s-%s', $m[2][0],
                                                        $m[2][1],
                                                        $m[2][2],
                                                        $m[2][3] ); 
                     
                     if ( check_is_updated( $version, $versions->modX->revolution ) )
                     {
                          echo "OK: ModX Revolution install found (updated) inside: $dir/$myfile - Version: $version \n";
                     }
                     else
                     {
                         echo "Warning: Found outdated ModX Revolution install inside: $dir/$myfile - Version: $version \n";
                     }
                }
               
            }
        }
        else if($myfile == "configuration.php")
        {
            $versionFile = '';
            if (file_exists("$dir/includes/version.php")) {
                $versionFile = "$dir/includes/version.php";
            }
            else if (file_exists("$dir/libraries/joomla/version.php")) {
                $versionFile = "$dir/libraries/joomla/version.php";
            }
            else if (file_exists("$dir/libraries/cms/version.php")) {
                $versionFile = "$dir/libraries/cms/version.php";
            }
            else if (file_exists("$dir/libraries/cms/version/version.php") && false === strpos( $dir, 'breezing-forms')) {
                $versionFile = "$dir/libraries/cms/version/version.php";
            }

            if ($versionFile !== '') {
                $realdir = dirname($dir . '/' . $myfile);
                $version1 = getfile($versionFile, "RELEASE");

                if ( !$version1 )
                {
                    continue;
                }

                $version1 = explode('\'', $version1);
                $version2 = getfile($versionFile, "DEV_LEVEL");
                $version2 = explode('\'', $version2);
                $version = $version1[1] . '.' .$version2[1];

                if ( check_is_updated( $version, $versions->joomla ) )
                {
                    echo "OK: Joomla install found (updated) inside: ".$realdir. " - Version: $version (from $versionFile).\n";
                }
                else
                {
                    echo "Warning: Found outdated Joomla install inside: ".$realdir. " - Version: $version (please update asap) - from $versionFile.\n";
                }
            }
        }
        else if ( $myfile == "jce.xml" )
        {
            $firstLine = getfile ( $dir . '/' .$myfile, 'install');

            if ( false === strpos( $firstLine, 'component' ) )
            {
                continue;
            }

            $version = getfile( $dir . "/" . $myfile, "<version>");
            $version = str_replace( '<version>', '', $version);
            $version = str_replace( '</version>', '', $version);
            $version = trim($version);
            $realdir = dirname($dir);

            if ( check_is_updated( $version, $versions->jce ) )
            {
                echo "OK: JCE component install found (updated) inside: $realdir - Version: $version (from $dir/$file).\n";
            }
            else
            {
                echo "Warning: Found outdated JCE component install inside: $realdir - Version: $version (please update asap) - from $dir/$myfile.\n";
            }
        }
        else if($myfile == "constants.php")
        {
            $version = getfile($dir."/".$myfile, "'PHPBB_VERSION'");

            if ( !$version )
            {
                continue;
            }

            $version = substr($version, 10);
            $version = trim($version);
            $version = str_replace(');', '', $version);
            $version = str_replace('\'', '', $version);
            $version = str_replace(' ', '', $version);
            $version = str_replace(',', '', $version);
            $version = str_replace('PHPBB_VERSION', '', $version);
            $version = str_replace('PBB_VERSION', '', $version);
            $version = trim($version);
            $realdir = dirname($dir);

            if ( check_is_updated( $version, $versions->phpbb ) )
            {
                echo "OK: PHPBB install found (updated) inside: ".$realdir. " - Version: $version (from $dir/$myfile).\n";
            }
            else
            {
                echo "Warning: Found outdated PHPBB install inside: ".$realdir. " - Version: $version (please update asap) - from $dir/$myfile.\n";
            }
        }

        else if($myfile == "diagnostic.php")
        {
            $version = getfile($dir."/".$myfile, "'vbulletin'");

            if ( !$version )
            {
                continue;
            }

            $version = str_replace("\t\t\$md5_sum_versions = array('vbulletin' => '", '', $version);
            $version = str_replace("');", '', $version);

            if ( check_is_updated( $version, $versions->vbulletin ) )
            {
                echo "OK: vBulletin version: ".$dir."/".$myfile." - $version\n";
            }
            else
            {
                echo "Warning: Found outdated vBulletin install inside: ".$dir."/".$myfile." - Version: $version\n";
            }
        }

        else if($myfile == $WPVERSION && strpos($dir, "administrator/components/com_jevents") === FALSE)
        {
            $version = getfile($dir."/".$myfile, "wp_version = ");
            if($version != NULL)
            {
                $realdir = dirname($dir);
                if($realdir == ".")
                {
                    $realdir = "/ (main folder)";
                }

                $explosion = explode( "'", $version );

                if ( isset( $explosion[1] ) )
                {
                    $version = $explosion[1];
                }

                if ( check_is_updated( $version, $versions->wordpress ) )
                {
                    echo "OK: WordPress install found (updated) inside: ".$realdir. " - Version: $version (from $dir/$myfile).\n";
                }
                else
                {
                    echo "Warning: Found outdated WordPress install inside: ".$realdir. " - Version: $version (please update asap) - from $dir/$myfile.\n";

                    if (
                         array_key_exists( 'wordpress', $software ) &&
                         array_key_exists( $version, $software['wordpress'] ) &&
                         array_key_exists( 'wpvundb', $_GET )
                       )  
                    {
                         print( "\nAssociated vulnerabilities:\n" );
                         foreach ( $software['wordpress'][$version] as $warning )
                         {
                             printf( "%s - %s\n", str_repeat( "\x20", 10 ), $warning );
                         }
                         print( "\n" );
                    }
                }
            }
            else
            {
                $version = getfile($dir."/".$myfile, "ZENPHOTO_VERSION");
                if($version != NULL)
                {
                    $version = trim($version);
                    $version = explode( '\'', $version);
                    $version = $version[3];

                    if ( check_is_updated( $version, $versions->zenphoto ) )
                    {
                        echo "OK: Zenphoto install found (updated) at: ". $dir . '/' . $myfile . " - Version: $version.\n";
                    }
                    else
                    {
                        echo "Warning: Found outdated Zenphoto install inside: $dir/$myfile - Version: $version (please update asap)\n";
                    }
                }


            }

        }

        else if((strpos($myfile, "timthumb") !== FALSE ||
                strpos($myfile, "thumb") !== FALSE ||
                strpos($myfile, "Thumb") !== FALSE ||
                strpos($myfile, "crop") !== FALSE) &&
                strpos($myfile, ".php") !== FALSE)

        {
            /* Check for timthumb version */
            if(getfile($dir."/".$myfile, "TimThumb"))
            {
                $version = getfile($dir."/".$myfile, "'VERSION'");

                if ( !$version )
                {
                    continue;
                }

                if(strpos($version, "'1.") !== FALSE ||
                 strpos($version, "'0.") !== FALSE)
                {
                    if (isset($_GET['ttupdate'])){
                        backup_file($dir.'/'.$myfile);
                        replace_timthumb($dir.'/'.$myfile);
                    }
                    else {
                    echo "Warning: Found outdated timthumb.php install inside: $dir/$myfile - Version: (below 2.0). Please update asap!\n";
                    }
                }
                else if(strpos($version, "'2.0") !== FALSE ||
                      strpos($version, "'2.1") !== FALSE ||
                      strpos($version, "'2.2") !== FALSE ||
                      strpos($version, "'2.3") !== FALSE ||
                      strpos($version, "'2.4") !== FALSE ||
                      strpos($version, "'2.5") !== FALSE ||
                      strpos($version, "'2.6") !== FALSE ||
                      strpos($version, "'2.7") !== FALSE ||
                      strpos($version, "'2.8'") !== FALSE ||
                      strpos($version, "'2.8.0") !== FALSE ||
                      strpos($version, "'2.8.1'") !== FALSE)
                {
                    if (isset($_GET['ttupdate'])){
                        backup_file($dir.'/'.$myfile);
                        replace_timthumb($dir.'/'.$myfile);
                    }
                    else {
                    echo "Warning: Found outdated timthumb.php install inside: $dir/$myfile - Version: (below 2.8.2). Update recommended.\n";
                    }
                }
                else
                {
                    $rversion = explode("'", $version);
                    if ( array_key_exists( 3, $rversion ) )
                    {
                        echo "OK: Found updated version (".$rversion[3].") of timthumb.php\n";
                    }
                }
            }
        }

        else if($myfile == $OSVERSION)
        {
            $config = @ file_get_contents( "$dir/$myfile" );
            if ( $config === false )
            {  
                 continue;
            }

            $version = getOption( 'PROJECT_VERSION', $config );  

            if ( !$version )
            {
                continue;
            }

            echo "osCommerce version: ".$dir."/".$myfile.": $version\n";
        }

        else if ( $myfile == 'uploadify.php' )
        {
           echo "Warning: uploadify.php found at $dir/$myfile . Please be sure that you have secured this plugin properly.\n";
        }

        else if ( $myfile == 'revslider.php' )
        {
            $version = getfile($dir."/".$myfile, "revSliderVersion");

            if(strpos($version, '"0.') !== FALSE || strpos($version, '"1.') !== FALSE || strpos($version, '"2.') !== FALSE || strpos($version, '"3.') !== FALSE ||
                  strpos($version, '"4.0') !== FALSE || strpos($version, '"4.1.3') !== FALSE ||
                  strpos($version, '"4.1.2') !== FALSE || strpos($version, '"4.1.1') !== FALSE || strpos($version, '"4.1.0') !== FALSE
                    || strpos($version, '"4.1.4') !== FALSE)
            {
                echo "Warning: vulnerable Slider Revolution plugin found at $dir/$myfile . Please update this plugin immediately: http://www.themepunch.com/home/plugin-update-information/ \n";
            }
        }
        else if ( $myfile == 'showbiz.php' )
        {
            $version = getfile($dir."/".$myfile, "showbizVersion");

            if(strpos($version, '"0.') !== FALSE || strpos($version, '"1.1') !== FALSE || strpos($version, '"1.2') !== FALSE || strpos($version, '"1.3') !== FALSE ||
              strpos($version, '"1.4') !== FALSE || strpos($version, '"1.5') !== FALSE ||
              strpos($version, '"1.6') !== FALSE || strpos($version, '"1.7.0') !== FALSE || strpos($version, '"1.7.1') !== FALSE)
            {
                echo "Warning: vulnerable ShowBiz Plugin found at $dir/$myfile . Please update this plugin immediately.\n";
            }
        }
        else if ( FALSE !== strpos( $dir, '/wp-content/plugins/' ) && FALSE !== strpos( $plugins, '*,*' . $myfile . '*,*' ) )
        {
            check_plugin( $dir, $myfile );
        }

        if(is_dir($dir."/".$myfile))
        {

            if(isset($_GET['noise']))
            {
                echo "    Reading Dir: $dir/$myfile\n";
            }
            scanallfiles($dir."/".$myfile);
            @flush();
        }
    }
    closedir($dh);
}

echo "<pre>\n";
echo "Sucuri version report: " . $myversion ."\n\n";
echo "PHP Version: ".phpversion()."\n\n";

if ( isset( $_SERVER['SERVER_ADDR'] ) )
{
     printf( "Server Addr: %s\n\n", $_SERVER['SERVER_ADDR'] );
}

/* Detect Hosting provider */
print("Hosting Provider: ");

$provider = @ gethostbyaddr( $_SERVER['SERVER_ADDR'] );
if ( false !== $provider )
{
     $providers = array();
     $providers['secureserver.net'] = 'GoDaddy';
     $providers['bluehost.com']     = 'BlueHost';
     $providers['hostgator.com']    = 'HostGator';
     $providers['site5.com']        = 'Site5';
     $providers['amazonaws.com']    = 'Amazon';
     $providers['siteground.com']   = 'Siteground';
     $providers['gridserver.com']   = 'MediaTemple';
     $providers['linode.com']       = 'WPEngine';
     $providers['1e100.net']        = 'Google';
     $providers['dreamhost.com']    = 'DreamHost';
    
     $match = false;
     foreach ( $providers as $host => $name )
     {
         if ( false !== strpos( $provider, $host ) )
         {
              $match = $name;
              break;
         }
     }
 
     if ( false !== $match )
     {
          print("$match\n\n");
     }
     else
     {
          print("Unknown provider - $provider.\n\n");
     }
}
else
{
     print("Unable to determine.\n\n"); 
}

print("CloudProxy Active: ");

$addr = @ gethostbyname( $_SERVER['HTTP_HOST'] );
$host = @ gethostbyaddr( $addr );
if ( preg_match( '@^cloudproxy[0-9]+\.sucuri\.net$@', $host ) )
{  
     print("Yes\n\n");
}
else
{
     print("No\n\n");
}

/* Scanning all files. */
$dir = "./";
if(isset($_GET['up']))
{
    $dir = "../";
}
if(isset($_GET['upup']))
{
    $dir = "../../";
}
if(isset($_GET['upupup']))
{
    $dir = "../../../";
}
if(isset($_GET['ttupdate']))
{
   $NewTimThumb = file_get_contents("http://timthumb.googlecode.com/svn/trunk/timthumb.php");
   function backup_file($file)
    {
        $backupcopy = $file."_sucuribackup.".time();
        if(!copy($file, $backupcopy))
        {
            return(0);
        }

        if(filesize($file) !=  filesize($backupcopy))
        {
            return(0);
        }
        chmod($backupcopy, 000);

        $newfile = file($file);
        if($newfile === FALSE || empty($newfile))
        {
            return(0);
        }
    }

    function replace_timthumb($file)
    {
       global $NewTimThumb;

       if (strpos($NewTimThumb, 'define (\'VERSION'))
     {
       if (!($fp = fopen($file, "w")))
       {
          echo "Couldn't open ". $file ."\n";
          return(0);
       }

       $writeFile = fwrite($fp, $NewTimThumb);

       if ($writeFile) {
            //echo "File Successfully written!\n";
        echo "UPDATED: Found outdated timthumb.php version at $file (below 2.8.2). And updated to the latest version.\n";
       } else {
            //echo "fwrite() failed.\n";
       echo "FAILED: Unable to update timthumb.php version at $file\n";
       }

       fclose($fp);

    }
     else {
            echo "FAILED: Unable to update timthumb.php version at $file - Error on downloading new version\n";
     }
   }
}

function getOption( $option, $config )
{
    if (
         ! is_string( $option ) ||
         ! is_string( $config )
       )
    {
         return null;
    }

    $option = preg_quote( $option );

    /* Constants */ 
    if ( preg_match( "@define\(\s*['\"]" . $option . "['\"]\s*,\s*['\"](.*)['\"]\s*\);@", $config, $m ) )
    {
         return $m[1];
    }

    /* Variables */
    if ( preg_match( "@" . $option . "\s*=\s*['\"](.*)['\"];@", $config, $m ) )
    {
         return $m[1];
    }
    
    /* Associative arrays */
    if ( preg_match( "@'" . $option . "'\s*=>\s*['\"](.*)['\"][,]?@", $config, $m ) )
    {
         return $m[1];
    }

    return null;
}

function scanDirectory( $dir )
{
    if (
         ! is_string( $dir ) ||
         ! is_dir( $dir )
       )
    {
         return null;
    }

    if (
         $dir == './'
       )
    {
         $dir = '.';
    }

    $data = array();
    if ( $handle = opendir( $dir ) )
    {
         while ( false !== ( $item = readdir( $handle ) ) )
         {
             if (
                  $item == '..' ||
                  $item == '.'
                )
             {
                  continue;
             }

             if ( $item == 'wp-config.php' )
             {
                  $data[] = array('type' => 'wordpress', 'dir' => $dir, 'config' => "$dir/$item" );
                  continue;
             }

             if ( $item == 'configuration.php' )
             {
                  $data[] = array('type' => 'joomla', 'dir' => $dir, 'config' => "$dir/$item");
                  continue;
             }

             if ( is_dir( "$dir/$item" ) )
             {
                  if ( is_file( "$dir/$item/wp-config.php" ) )
                  {
                       $data[] = array('type' => 'wordpress', 'dir' => "$dir/$item", 'config' => "$dir/$item/wp-config.php" );
                       continue;
                  }

                  if ( is_file( "$dir/$item/configuration.php" ) )
                  {
                       $data[] = array('type' => 'joomla', 'dir' => "$dir/$item", 'config' => "$dir/$item/configuration.php");
                       continue;
                  }
             }
         }

         closedir( $handle );
    }
    return $data;
}

if ( isset( $_GET['list-plugins'] ) )
{
    $items = scanDirectory( $dir );

   if ( is_array( $items ) )
   {
     foreach ( $items as $item )
     {
         if ( is_readable( $item['config'] ) )
         {
              $config = file_get_contents( $item['config'] );

              print str_repeat( '-', 100 ) . "\n";

              if ( $item['type'] == 'wordpress' )
              {
                   print("Found wordpress at: " . $item["dir"] . "\n");

                   $dbHost = getOption( 'DB_HOST', $config );
                   $dbUser = getOption( 'DB_USER', $config );
                   $dbPass = getOption( 'DB_PASSWORD', $config );
                   $dbName = getOption( 'DB_NAME', $config );
                   $prefix = getOption( '$table_prefix', $config );
              }
              else if ( $item['type'] == 'joomla' )
              {
                   print("Found joomla at: " . $item["dir"] . "\n");

                   $dbHost = getOption( '$host', $config );
                   $dbUser = getOption( '$user', $config );
                   $dbPass = getOption( '$password', $config );
                   $dbName = getOption( '$db', $config );
                   $prefix = getOption( '$dbprefix', $config );
              }
              else
              {
                   continue;
              }

              print( "\n" );

              if (
                   ( $link = @ mysql_connect( $dbHost, $dbUser, $dbPass ) ) &&
                             @ mysql_select_db( $dbName, $link )
                 )
              {
                   if ( $item['type'] == 'wordpress' )
                   {
                        if ( ! $res = mysql_query( "SELECT `option_value` FROM `{$prefix}options` WHERE `option_name` = '_transient_plugin_slugs'", $link ) )
                        {
                             print( "An error occurred, skipping plugins.\n" );

                             continue;
                        }

                        if ( ! $row = mysql_fetch_assoc( $res ) )
                        {
                             print("No Plugins found.\n");
                        }
                        else
                        {
                             print("[Plugins]\n\n");

                             $plugins = unserialize( $row['option_value'] );

                             if ( is_array( $plugins ) )
                             {
                                  $_plugins = array();

                                  foreach ( $plugins as $plugin )
                                  {
                                      $version = null;
                                      $matched = false;
                                      if ( is_file( "$item[dir]/wp-content/plugins/$plugin" ) )
                                      {
                                           $version = get_wp_plugin_version( "$item[dir]/wp-content/plugins/$plugin" );
                                           $matched = true;
                                      }

                                      if ( ! preg_match( '/([\d\.]+)/', $version, $m ) )
                                      {
                                           $version = 'N/A';
                                      }

                                      if ( preg_match( '@^([^/]+)/.+$@ ', $plugin, $m  ) )
                                      {
                                           $plugin = $m[1];
                                      }
                                      else
                                      {
                                           $plugin = basename( $plugin );
                                      }

                                      if ( $matched )
                                      {
                                           $update = "Could not contact wordpress to check for update.";
                                           $order  = 3;
                                           if ( $data = @ file_get_contents( "https://api.wordpress.org/plugins/info/1.0/$plugin" ) )
                                           {
                                                $data = (array) unserialize( $data );

                                                if ( isset( $data['version'] ) && version_compare( $data['version'], $version, '>' ) )
                                                {
                                                     $update = "There is a new version available: " . $data['version'];
                                                     $order  = 0;
                                                }
                                                else if ( isset( $data['version'] ) && $data['version'] === $version )
                                                {
                                                     $update = "You are up to date.";
                                                     $order  = 1;
                                                }
                                                else
                                                {
                                                     $update = "Possible premium plugin ?";
                                                     $order  = 2;
                                                }
                                            }
                                      }
                                      else
                                      {
                                          $update = "Skipping check, broken plugin.";
                                          $order  = 3;
                                      }

                                      $_plugins[$order][] = sprintf( "Found wordpress plugin - %s - version: (%s) - %s\n", htmlspecialchars( $plugin ),
                                                                                                                           htmlspecialchars( $version ),
                                                                                                                           htmlspecialchars( $update ) );
                                  }

                                  foreach ( $_plugins as $order => $arr )
                                  {
                                      switch ( $order )
                                      {
                                          case 0:
                                               print("Outdated plugins:\n\n");
                                               break;
                                          case 1:
                                               print("Updated plugins:\n\n");
                                               break;
                                          case 2:
                                               print("Possible premium plugins:\n\n");
                                               break;
                                          case 3:
                                               print("Errors:\n\n");
                                               break;
                                      }

                                      foreach ( $arr as $_item )
                                      {
                                          print($_item);
                                      }
                                      print("\n");
                                  }

                                  unset( $_plugins );
                             }
                        }

                        print("\n");

                        if ( ! $res = mysql_query( "SELECT `option_value` FROM `{$prefix}options` WHERE `option_name` = '_site_transient_theme_roots'", $link ) )
                        {
                             print( "An error occurred, skipping themes.\n" );

                             continue;
                        }

                        if ( ! $row = mysql_fetch_assoc( $res ) )
                        {
                             print("No themes found.\n");
                        }
                        else
                        {
                             print("[Themes]\n\n");

                             $themes = unserialize( $row['option_value'] );

                             $_themes = array();

                             if ( is_array( $themes ) )
                             {
                                  foreach ( $themes as $theme => $_ )
                                  {
                                      $version = null;
                                      $matched = false;
                                      if ( is_file( "$item[dir]/wp-content$_/$theme/style.css" ) )
                                      {
                                           $version = get_wp_plugin_version( "$item[dir]/wp-content$_/$theme/style.css" );
                                           $matched = true;
                                      }

                                      if ( ! preg_match( '/([\d\.]+)/', $version ) )
                                      {
                                           $version = 'N/A';
                                      }

                                      if ( $matched  )
                                      {
                                         $update = "Could not contact wordpress.";
                                          $order  = 3;
                                          if ( $data = @ file_get_contents( "https://api.wordpress.org/themes/info/1.1/?action=theme_information&request[slug]=$theme" ) )
                                          {
                                               $data = json_decode( $data, true );
                                               if ( isset( $data['version'] ) && version_compare( $data['version'], $version, '>' ) )
                                               {
                                                    $update = "There is a new version available: " . $data['version'];
                                                    $order  = 0;
                                               }
                                               else if ( isset( $data['version'] ) && $data['version'] === $version )
                                               {
                                                    $update = "You are up to date.";
                                                    $order  = 1;
                                               }
                                               else
                                               {
                                                    $update = "Possible premium theme?";
                                                    $order  = 2;
                                               }
                                           }
                                      }
                                      else
                                      {
                                           $update = "Skipping check, broken theme.";
                                           $order  = 3;
                                      }

                                     $_themes[$order][] = sprintf( "Found wordpress theme - %s - version: (%s) - %s\n", htmlspecialchars( $theme ),
                                                                                                                        htmlspecialchars( $version ),
                                                                                                                        htmlspecialchars( $update ) );
                                  }

                                  foreach ( $_themes as $order => $arr )
                                  {
                                      switch ( $order )
                                      {
                                          case 0:
                                               print("Outdated themes:\n\n");
                                               break;
                                          case 1:
                                               print("Updated themes:\n\n");
                                               break;
                                          case 2:
                                               print("Possible premium themes:\n\n");
                                               break;
                                          case 3:
                                               print("Errors:\n\n");
                                               break;
                                      }

                                      foreach ( $arr as $_item )
                                      {
                                          print($_item);
                                      }
                                      print("\n");
                                  }

                                  unset( $_themes );
                             }
                        }

                        print str_repeat( '-', 100 ) . "\n\n";
                   }
                   else if ( $item['type'] == 'joomla' )
                   {
                        if ( ! $res = mysql_query("SELECT `name`, `type`, `manifest_cache` FROM `{$prefix}extensions` ORDER BY `type`, `name`", $link) )
                        {
                             print("An error occurred, skipping.\n");

                             continue;
                        }

                        $group = null;
                        while ( $row = mysql_fetch_assoc( $res ) )
                        {
                             if ( ! $group || $row['type'] !== $group )
                             {
                                  $group = $row['type'];
                                  print("\n");
                             }

                             $row['manifest_cache'] = json_decode( $row['manifest_cache'], true );

                             if ( ! isset( $row['manifest_cache']['version'] ) )
                             {
                                  $row['manifest_cache']['version'] = 'N/A';
                             }

                             printf( "Found joomla %s - %s - version: (%s)\n", htmlspecialchars( $row['type'] ),
                                                                               htmlspecialchars( $row['name'] ),
                                                                               htmlspecialchars( $row['manifest_cache']['version'] ) );
                        }
                   }

                   @ mysql_close( $link );
              }
              else
              {
                   print("Couldn't establish db connection.\n");
              }

              print( "\n" );
         }
         else
         {
              print("Skipping unreadable config " . $item['type'] . " .\n");
         }
     }
  }
  else
  {
     print("Nothing found.\n");
  }
}
else
{
      scanallfiles($dir);
}

echo "\nCompleted.\n";
echo "</pre>\n";
exit( 0 );
?>



