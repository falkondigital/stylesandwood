<?php

define('SH_NAME', 'wp_craper');
define('SH_VERSION', '1.0');
define('SH_ROOT', get_template_directory().'/');
define('SH_URL', get_template_directory_uri().'/');

add_action('after_setup_theme', 'sh_theme_setup');
add_filter( 'automatic_updates_is_vcs_checkout', '__return_false', 1 );
include('includes/loader.php');

function sh_theme_setup()
{
	global $wp_version, $pagenow;

	load_theme_textdomain(SH_NAME, get_template_directory() . '/languages');
	

	//ADD THUMBNAIL SUPPORT
	add_editor_style('css/bootstrap.css');
	
	add_theme_support('post-thumbnails');
	add_theme_support('menus'); //Add menu support
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	add_theme_support( 'custom-background' ); 
	
	add_theme_support( 'post-formats', array( 'video', 'gallery', 'audio', 'quote', 'status' ), array('post',) );
	
	
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => __('Main Menu', SH_NAME),
				//'footer_menu' => __('Footer Menu', SH_NAME),
			)
		);
	}
	
	if ( ! isset( $content_width ) ) $content_width = 960;
	
	add_image_size( '600x410', 600, 410, true );
	add_image_size( '268x268', 268, 268, true );
	add_image_size( '800x547', 800, 547, true );
	add_image_size( '1750x1143', 1750, 1143, true );
	add_image_size( '75x75', 75, 75, true );
	add_image_size( '392x523', 392, 523, true );
	add_image_size( '306x250', 306, 250, true );
	add_image_size( '80x80', 80, 80, true );
	add_image_size( '270x220', 270, 220, true );
	add_image_size( '770x600', 770, 600, true );
	add_image_size( '270x237', 270, 237, true );
	add_image_size( '266x75', 266, 75, true );
	add_image_size( '230x230', 230, 230, true );
	add_image_size( '200x200', 200, 200, true );
	add_image_size( '370x300', 370, 300, true );
	add_image_size( '100x100', 100, 100, true );
	add_image_size( '770x388', 770, 388, true );

		
}

function sh_widget_init()
{
	global $wp_registered_sidebars;

	register_widget( 'SH_GetinTouch' );
	register_widget( 'SH_RecentPosts' );
	register_widget( 'SH_Gallery' );
	register_widget( 'SH_FindUsHere' );
	
	
	register_sidebar( array(
		'name' => __('Default Sidebar',SH_NAME),
		'id' => 'default-sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="title"><span>',
		'after_title' => '</span></h4>',
	) );
	
	register_sidebar( array(
		'name' => __('Blog Sidebar',SH_NAME),
		'id' => 'blog',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="title"><span>',
		'after_title' => '</span></h4>',
	) );
	
	register_sidebar( array(
		'name' => __('Contact Sidebar',SH_NAME),
		'id' => 'contact',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="title"><span>',
		'after_title' => '</span></h4>',
	) );
		
	register_sidebar( array(
		'name' => __('Footer Sidebar',SH_NAME),
		'id' => 'footer-sidebar',
		'before_widget' => '<div id="%1$s" class="col-xs-12 col-sm-6 col-md-3 animated out %2$s" data-animation="fadeInUp" data-delay="0">',
		'after_widget' => "</div>",
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );	
	
	sh_register_dynamic_sidebar();

	update_option('wp_registered_sidebars', $wp_registered_sidebars);
	
	
}

add_action('widgets_init', 'sh_widget_init' );


/** Enqueue scripts and style **/ 
class SH_Enqueue
{
	var $opt;
	
	function __construct()
	{
		$this->opt = get_option(SH_NAME);
		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );
		
		add_action( 'wp_head', array( $this, 'sh_wp_head' ) );
		
		add_action( 'wp_footer', array( $this, 'sh_wp_footer' ) );
		
		add_action('admin_enqueue_scripts', array($this, 'sh_enqueue') );
	}

	function sh_enqueue_scripts()
	{
		$protocol = is_ssl() ? 'https' : 'http';
				$styles = array('font-awesome' => 'css/fonts/font-awesome.css',
						'font-typicons' => 'css/fonts/typicons.css',
						'ico-font' => 'css/fonts/ico-font.css',
						'revolution-slider' => 'css/revolution-slider/settings.css',
						'bootstrap' => 'css/bootstrap.css',
						'style' => 'css/style.css',
						'color' => 'css/color.css',
						'animate' => 'css/animate.css',
						'prettyPhoto' => 'css/prettyPhoto.css',
						'custome' => 'css/custome.css',
						'responsive'=>'css/responsive.css',
						'main_style'=>get_stylesheet_directory_uri().'/style.css',);
		
	
		$styles = $this->custom_fonts($styles);
		
		foreach( $styles as $name => $style )
		{
			if(strstr($style, 'http') || strstr($style, 'https')) wp_register_style( $name, $style);
			else {
				wp_register_style( $name, SH_URL.$style, '', null);
			}
			
			wp_enqueue_style($name);
		}
		
				
		$scripts = array(
						'jquery-ui-1-10-3-custom-min' => 'jquery-ui-1.10.3.custom.min.js',
						'jquery-appear' => 'jquery.appear.js',
						 'jquery-carouFredSel-6-2-1-packed' => 'jquery.carouFredSel-6.2.1-packed.js',
						 'jquery-knob' => 'jquery.knob.js',
						 'jquery-mixitup-min' => 'jquery.mixitup.min.js',
						 'jquery-prettyPhoto' => 'jquery.prettyPhoto.js',
						 'jquery-roundabout-min' => 'jquery.roundabout.min.js',
						 'jquery-roundabout-shapes-min' => 'jquery.roundabout-shapes.min.js',
						 'share-this' => 'addthis.js',
						 'share-this-script' => 'addthis-script.js',

						 'masonry-pkgd-min' => 'masonry.pkgd.min.js',
						 'script' => 'script.js');
		
		wp_enqueue_script(array('jquery', 'jquery-ui-tooltip'));
		
		foreach( $scripts as $name => $js ) wp_register_script( $name, SH_URL.'js/'.$js, '', SH_VERSION, true);
		
		wp_enqueue_script( array('jquery', 'jquery-appear', 'jquery-carouFredSel-6-2-1-packed', 'jquery-knob' ,'jquery-mixitup-min', 
								 'jquery-prettyPhoto', 'jquery-roundabout-min', 'jquery-roundabout-shapes-min',
								 'jquery-ui-1-10-3-custom-min',
								 'masonry-pkgd-min', 'script', 'share-this', 'share-this-script',  
						) );
		
		if( is_singular() ) wp_enqueue_script('comment-reply');
		
		sh_theme_color_scheme();

	}
	
	function sh_wp_head()
	{
		$opt = _WSH()->option();
		echo '<script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';?>
		<style type="text/css">
			<?php
			if( sh_set( $opt, 'body_custom_font') )
			echo sh_get_font_settings( array( 'body_font_size' => 'font-size', 'body_font_family' => 'font-family', 'body_font_style' => 'font-style', 'body_font_color' => 'color', 'body_line_height' => 'line-height' ), 'body, p {', '}' );
			
			if( sh_set( $opt, 'use_custom_font' ) ){
			echo sh_get_font_settings( array( 'h1_font_size' => 'font-size', 'h1_font_family' => 'font-family', 'h1_font_style' => 'font-style', 'h1_font_color' => 'color', 'h1_line_height' => 'line-height' ), 'h1 {', '}' );
			echo sh_get_font_settings( array( 'h2_font_size' => 'font-size', 'h2_font_family' => 'font-family', 'h2_font_style' => 'font-style', 'h2_font_color' => 'color', 'h2_line_height' => 'line-height' ), 'h2 {', '}' );
			echo sh_get_font_settings( array( 'h3_font_size' => 'font-size', 'h3_font_family' => 'font-family', 'h3_font_style' => 'font-style', 'h3_font_color' => 'color', 'h3_line_height' => 'line-height' ), 'h3 {', '}' );
			echo sh_get_font_settings( array( 'h4_font_size' => 'font-size', 'h4_font_family' => 'font-family', 'h4_font_style' => 'font-style', 'h4_font_color' => 'color', 'h4_line_height' => 'line-height' ), 'h4 {', '}' );
			echo sh_get_font_settings( array( 'h5_font_size' => 'font-size', 'h5_font_family' => 'font-family', 'h5_font_style' => 'font-style', 'h5_font_color' => 'color', 'h5_line_height' => 'line-height' ), 'h5 {', '}' );
			echo sh_get_font_settings( array( 'h6_font_size' => 'font-size', 'h6_font_family' => 'font-family', 'h6_font_style' => 'font-style', 'h6_font_color' => 'color', 'h6_line_height' => 'line-height' ), 'h6 {', '}' );
			}
			
			echo sh_set( $opt, 'header_css' );
			?>
		</style>
        
        <?php if( sh_set( $opt, 'header_js' ) ): ?>

			<script type="text/javascript">
                <?php echo sh_set( $opt, 'header_js' );?>
            </script>

        <?php endif;?>
        <?php 
	}
	
	function sh_wp_footer()
	{
		$analytics = sh_set( $this->opt, 'footer_analytics');
		
		echo $analytics;
		
		$theme_options = _WSH()->option();
		
		if( sh_set( $theme_options, 'footer_js' ) ): ?>
			<script type="text/javascript">
                <?php echo sh_set( $theme_options, 'footer_js' );?>
            </script>
        <?php endif;
	}
	
	function sh_enqueue()
	{
		wp_register_script('jquery-select2', SH_URL.'includes/resource/js/jquery.select2.min.js');
		
		wp_register_style('jquery-ui-datepicker-custom', SH_URL.'includes/resource/css/jquery.ui.all.css');
		wp_register_style('admin-custom-styles', SH_URL.'includes/resource/css/style.css');
	}

	function custom_fonts( $styles )
	{
		$opt = _WSH()->option();
		$protocol = ( is_ssl() ) ? 'https' : 'http';

		$font = array();
		
		if( sh_set( $opt, 'use_custom_font' ) ){
			
			if( $h1 = sh_set( $opt, 'h1_font_family' ) ) $font[$h1] = urlencode( $h1 ).':400,300,600,700,800';
			if( $h2 = sh_set( $opt, 'h2_font_family' ) ) $font[$h2] = urlencode( $h2 ).':400,300,600,700,800';
			if( $h3 = sh_set( $opt, 'h3_font_family' ) ) $font[$h3] = urlencode( $h3 ).':400,300,600,700,800';
		}
		
		if( sh_set( $opt, 'body_custom_font' ) ){
			if( $body = sh_set( $opt, 'body_font_family' ) ) $font[$body] = urlencode( $body ).':400,300,600,700,800';
		}
		
		if( $font ) $styles['sh_google_custom_font'] = $protocol.'://fonts.googleapis.com/css?family='.implode('|', $font);
		
		return $styles;
	}
}
 
 
//new SH_Enqueue;
