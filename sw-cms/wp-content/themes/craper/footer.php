
<?php
if(get_the_ID() != 6223){
	?>
	<footer id="footertop">
		<div class="container">
			<div class="row">

				<div class="col-md-4">

                    <h3>Useful Links</h3>

                    <p>
                        <a href="http://www.stylesandwood-group.co.uk/" target="_blank" rel="nofollow">Styles&amp;Wood Group</a><br>
                        <a href="http://www.stylesandwood.co.uk/" target="_blank" rel="nofollow">Styles&amp;Wood</a><br>
                        <a href="http://www.keysource.co.uk/ " target="_blank" rel="nofollow">Keysource</a><br>
                        <a href="http://www.gdmpartnership.co.uk/ " target="_blank" rel="nofollow">GDM Partnership</a><br>
                    </p>

<!--					<h3>Our Locations</h3>-->
<!--					<p><a target="_blank" href="https://www.google.co.uk/maps/place/Sale+M33+7BU/@53.4320076,-2.3197338,17z/data=!3m1!4b1!4m5!3m4!1s0x487bac4a68eb729f:0x91f730091ce0a0af!8m2!3d53.4318861!4d-2.3174023">Manchester</a><br />-->
<!--						<a target="_blank" href="https://www.google.co.uk/maps/place/Ruddington,+Nottingham+NG11+6JS/@52.8816172,-1.1499248,16z/data=!3m1!4b1!4m5!3m4!1s0x4879c326fd866609:0xf6659e408dd09682!8m2!3d52.8825237!4d-1.1433252" >Nottingham</a><br />-->
<!--						<a target="_blank" href="https://www.google.co.uk/maps/place/Charterhouse+St,+London+EC1M+6HR/@51.5197396,-0.1034548,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b53de1fdc97:0xdc2000fc8b74d4f2!8m2!3d51.5200217!4d-0.1013187">London</a></p>-->
<!---->
<!--								<h3>Useful Links</h3>-->
<!--									<p><a href="/disclaimer/">Disclaimer</a><br />-->
<!--					<!--				<a href="/sitemap/">Sitemap</a><br />-->
<!--									<a href="/contactus/">Contact Us</a></p>-->
				</div>

<!--				--><?php //dynamic_sidebar('footer-sidebar'); ?>


                <div class="col-md-4">

                    <br>
                    <p style="padding-top:16px;">
                    <a href="<?php echo home_url(); ?>/contactus/">Contact Us</a><br>
                    <a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br>
                    <a href="http://www.stylesandwood.co.uk/joinus/" target="_blank">Vacancies</a><br>
                        <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Modern-Slavery-Statement.pdf" target="_blank">Modern Slavery Statement </a><br>
                        <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Health-Safety.pdf" target="_blank">Health & Safety Policy </a><br>
                        <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Quality-Policy.pdf" target="_blank">Quality Policy </a><br>
                        <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Environmental-Policy.pdf" target="_blank">Environmental Policy</a><br>
						<a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2018/03/StylesWood-Gender-Pay_FIN_web.pdf" target="_blank">Gender Pay Policy</a>
                    </p>
                </div>

				<div class="col-md-4">
<!--					<h3>Share Price</h3>
					<iframe class="share-iframe" frameborder=0 src="http://ir.tools.investis.com/Clients/uk/styles_and_wood1/Ticker/Ticker.aspx?culture=en-GB"></iframe><br>-->
				</div>


		</div>


		<div class="container">

			<div class="row">
				<div class="span12">
					<div class=" fixclear">
						<ul class="social-icons colored fixclear"><li class="title">See more from the Styles&amp;Wood Group</li><li class="social-twitter"><a target="_blank" href="https://twitter.com/StylesandWood">Twitter</a></li><li class="social-linkedin"><a target="_blank" href="http://www.linkedin.com/company/78033">LinkedIn</a></li></ul>
						<div class="copyright">
							<a href="http://www.stylesandwood-group.co.uk"><img alt="Styles&amp;Wood Group" src="<?php echo get_site_url();?>/wp-content/uploads/SylesWood-sm.png"></a><p>&copy; <?php echo date('Y');?> Copyright - Styles&amp;Wood Group</p>
						</div><!-- end copyright -->



					</div><!-- end bottom -->
				</div>
			</div>

		</div>
	</footer>
<?php } ?>
</div>
<!-- /pageWrapper -->

<?php wp_footer(); ?>

<!-- Don't forget analytics -->

</body>

</html>
