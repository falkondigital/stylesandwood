<?php 
$theme_options = _WSH()->option(); 
$sidebar = sh_set( $theme_options, 'blog_sidebar');
$layout = sh_set($theme_options, 'blog_layout');
$column = ( $layout == 'full' ) ? 'col-3' : 'col-2';

/// added by adi
$column =  'col-2';
?>
<div class="<?php echo $column;?>">
	<ul class="masonry-grid">
	
		<?php while( have_posts() ): the_post(); ?>
	
            <li>
                <div id="post-<?php the_ID(); ?>" <?php post_class( 'blog-box animated out' ); ?> data-animation="fadeInUp" data-delay="0">
                    
                    <?php $no_image = ( !has_post_thumbnail() ) ? ' no-image' : ''; ?>
                    <figure class="image image-hover<?php echo $no_image; ?>"> 
                        
                        <?php if( has_post_thumbnail() ): ?>
                            <a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('370x300');?></a>
                        <?php endif; 
						
					/*				   
		 <figcaption>
				<a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>">        
				<h5><?php the_title();?></h5></a>

				<p><?php echo the_category(', '); ?></p>
			</figcaption>
		</figure>
					  */ ?>
                    <header class="clearfix">
                        <h3><a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a></h3>
                        <p class="meta pull-left"><?php _e('posted on ', SH_NAME); ?><?php echo get_the_date(); ?></p>
                        <ul class="options pull-right">
                            <li>
                                <a href="<?php the_permalink(); ?>#comments" title="<?php _e('Comments on ', SH_NAME); ?><?php the_title_attribute(); ?>">
                                    <i class="icon-chat-01"></i><?php comments_number();?>
                                </a>
                            </li>
                        </ul>
                    </header>
                    
                    <?php the_excerpt();?>
                    
                </div>
            </li>
		
		<?php endwhile; ?>
		
	</ul>
</div>