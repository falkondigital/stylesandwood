<?php
	if(get_the_ID() != 6223){
?>
 <footer id="footertop">
        <div class="container">
            <div class="row">
			
			<div class="col-md-4">
			
			<h3>Our Locations</h3> 
				<p><a target="_blank" href="https://www.google.co.uk/maps/place/Sale+M33+7BU/@53.4320076,-2.3197338,17z/data=!3m1!4b1!4m5!3m4!1s0x487bac4a68eb729f:0x91f730091ce0a0af!8m2!3d53.4318861!4d-2.3174023">Manchester</a><br />
				<a target="_blank" href="https://www.google.co.uk/maps/place/Ruddington,+Nottingham+NG11+6JS/@52.8816172,-1.1499248,16z/data=!3m1!4b1!4m5!3m4!1s0x4879c326fd866609:0xf6659e408dd09682!8m2!3d52.8825237!4d-1.1433252" >Nottingham</a><br />
				<a target="_blank" href="https://www.google.co.uk/maps/place/Charterhouse+St,+London+EC1M+6HR/@51.5197396,-0.1034548,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b53de1fdc97:0xdc2000fc8b74d4f2!8m2!3d51.5200217!4d-0.1013187">London</a></p>
			
			<h3>Useful Links</h3>			
				<p><a href="/disclaimer/">Disclaimer</a><br />
				<a href="/sitemap/">Sitemap</a><br />
				<a href="/contactus/">Contact Us</a></p>
			</div>
	
			<?php dynamic_sidebar('footer-sidebar'); ?>

		<div class="col-md-4">
		<h3>Share Price</h3>		
			 <p id="sharebox"><?php include('stock2014.php'); ?></p>
		</div>
		
		
				<div class="col-md-4">
		<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; }
	#mce-success-response{color:#fff;}
	#mc_embed_signup div.response{padding:0px;}
</style>
<div id="mc_embed_signup">
<form action="//stylesandwood-group.us8.list-manage.com/subscribe/post?u=54bef7046832cb4cfa0334e98&amp;id=25bf41502e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME">First Name </label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_54bef7046832cb4cfa0334e98_25bf41502e" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe to our mailing list" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
		</div>
		
		
		</div>
		</div>

		 
        <div class="container">
        
		<div class="row">
				<div class="span12">
					<div class=" fixclear">
					<ul class="social-icons colored fixclear"><li class="title">See more from the Styles&amp;Wood Group</li><li class="social-twitter"><a target="_blank" href="https://twitter.com/StylesandWood">Twitter</a></li><li class="social-linkedin"><a target="_blank" href="http://www.linkedin.com/company/78033">LinkedIn</a></li></ul>					
						<div class="copyright">
							<a href="http://www.stylesandwood-group.co.uk"><img alt="Styles&amp;Wood Group PLC" src="http://stylesandwood-group.co.uk/sw-cms/wp-content/uploads/SylesWood-sm.png"></a><p>&copy; 2017 Copyright - Styles&amp;Wood Group PLC</p>							
						</div><!-- end copyright -->
							
											

					</div><!-- end bottom -->
				</div>
			</div>
		
		</div>
    </footer>
	<?php } ?>
</div>
<!-- /pageWrapper -->

<?php wp_footer(); ?>

<!-- Don't forget analytics -->
	
</body>

</html>
