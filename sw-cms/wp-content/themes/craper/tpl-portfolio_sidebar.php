<?php /* Template Name: Portfolio with sidebar */ 
get_header(); ?>
<?php
$home='not';
if(is_home() || is_front_page()) $home = 'home';
$settings = get_post_meta(get_the_ID(), '_craper_page_meta', true);
$sidebar = sh_set( $settings, 'page_sidebar' );
if(!$sidebar){
	$theme_options = _WSH()->option();
	$sidebar = sh_set( $theme_options, 'page_sidebar' );
	$layout = sh_set( $theme_options, 'blog_layout' );
	$class = (($home=='not')&&($layout != 'full')) ? 'col-md-8' : 'col-md-12';	
	
}else{
	
	$layout = sh_set( $settings, 'layout' );
	$class = (($home=='not')&&($layout != 'full')) ? 'col-md-8' : 'col-md-12';
}

?>
<article class="banner">
	<div class="container">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
			<?php echo get_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</article>
<article role="main">
        <div class="section">
            <div class="container">
				<div class="row">

		<?php if( ($home=='not')&&($layout == 'left') ): ?>
        	<div class="col-xs-12 col-sm-4 col-md-4 sidebar"><?php dynamic_sidebar( $sidebar ); ?></div>
        <?php endif; ?>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="col-xs-12 col-sm-8 <?php echo $class; ?>">
			
				<?php the_content(); ?>
			
			</div>

		<?php endwhile; endif; ?>

		<?php if( ($home=='not')&&($layout == 'right') ): ?>
            <div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                <?php dynamic_sidebar( $sidebar ); ?>
            </div>
        <?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
