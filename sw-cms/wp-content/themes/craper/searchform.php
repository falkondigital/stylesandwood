<form action="<?php echo home_url(); ?>" id="searchform" method="get">
    <div>
        <input type="text" id="s" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php _e('search for something...', SH_NAME); ?>" />
		<input type="submit" value="Search" id="searchsubmit" />
	</div>
</form>