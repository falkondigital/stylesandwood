<?php get_header(); 


if( $wp_query->is_posts_page )
{
	
	$quereid_object = get_queried_object();

	$meta = get_post_meta($quereid_object->ID, '_craper_page_meta', true);
	$sidebar = sh_set( $meta, 'page_sidebar' );
	$layout = sh_set($meta, 'layout');
}else {
	$theme_options = array();//_WSH()->option(); 
	$sidebar = sh_set( $theme_options, 'blog_sidebar', 'default-sidebar' );
	$layout = sh_set($theme_options, 'blog_layout', 'right');
}


?>

<article class="banner">
	<div class="container">
		<h2><?php _e( 'Latest', SH_NAME ); ?></h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
				<?php echo get_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</article>

<article role="main">
	<div class="section">
		<div class="container">
			<div class="row">
				
				<?php
$layout = 'right';
$sidebar = 'blog';


				$class = ( $layout == 'full' ) ? 'col-md-12' : 'col-xs-12 col-sm-8 col-md-8';
					if( $layout == 'left' ): ?>
						<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    		<?php dynamic_sidebar( $sidebar ); ?>
                		</div>
            	<?php endif; ?>

				
				<div class="<?php echo $class; ?>">

					<?php if( $layout == 'full' ): ?>
					<div class="blog-centered">
					<?php endif; ?>

					<div class="contents">
						
						<?php $layout = 'right';
						//get_template_part('content', 'blog'); 
						get_template_part('content', 'masonry'); 
						?>
						<?php _the_pagination(); ?>
						
					</div>
					
					<?php if( $layout == 'full' ): ?>
					</div>
					<?php endif; ?>

				</div>				
				
				<?php if( $layout == 'right' ): ?>
					
                    <div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    	<?php dynamic_sidebar( $sidebar ); ?>
                	</div>
            	
				<?php endif; ?>

			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
