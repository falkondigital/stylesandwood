<?php
//How we work
vc_map( array(
			"name" => __("How we work", SH_NAME),
			"base" => "sh_how_we_work",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for How we work section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of work to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

//Portfolio
vc_map( array(
			"name" => __("Portfolio", SH_NAME),
			"base" => "sh_portfolio",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for Portfolio section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of portfolio to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			    array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "button_text",
				   "description" => __("Enter the portfolio button text", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "button_link",
				   "description" => __("Enter the portfolio button link", SH_NAME)
				),
			)
	    )
);

//Block title
vc_map( array(
			"name" => __("Block Title", SH_NAME),
			"base" => "sh_block_title",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for for the block", SH_NAME)
				),
			)
	    )
);

//testimonial
vc_map( array(
			"name" => __("Testimonial", SH_NAME),
			"base" => "sh_testimonial",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of testimonial to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'testimonial_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Pagination", SH_NAME),
				   "param_name" => "pagination",
				   'value' => array_flip( array('top'=>__('Top', SH_NAME),'bottom'=>__('Bottom', SH_NAME) ) ),			
				   "description" => __("Enter the Pagination location.", SH_NAME)
				),
			)
	    )
);

//clients
vc_map( array(
			"name" => __("Clients", SH_NAME),
			"base" => "sh_clients",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of clients to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
			)
	    )
);

//Ready to start
vc_map( array(
			"name" => __("Ready to start", SH_NAME),
			"base" => "sh_ready_to_start",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for ready to start section", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for ready to start section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Left Button Text", SH_NAME),
				   "param_name" => "left_btn_text",
				   "description" => __("Enter the Left button text for ready to start section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Left Button Link", SH_NAME),
				   "param_name" => "left_btn_link",
				   "description" => __("Enter the Left button link for ready to start section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Right Button Text", SH_NAME),
				   "param_name" => "right_btn_text",
				   "description" => __("Enter the Right button text for ready to start section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Right Button Link", SH_NAME),
				   "param_name" => "right_btn_link",
				   "description" => __("Enter the Right button link for ready to start section", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Image", SH_NAME),
				   "param_name" => "img",
				   "description" => __("Choose the section background Image.", SH_NAME)
				),

			)
	    )
);
//Some features
vc_map( array(
			"name" => __("Some features title", SH_NAME),
			"base" => "sh_some_features",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for some features section", SH_NAME)
				),
			)
	    )
);

//feel the power
vc_map( array(
			"name" => __("Feel the power", SH_NAME),
			"base" => "sh_feel_the_power",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for some features section", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for some features section", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Image", SH_NAME),
				   "param_name" => "img",
				   "description" => __("Choose the section Image.", SH_NAME)
				),
			)
	    )
);

//Features list
vc_map( array(
			"name" => __("Features list", SH_NAME),
			"base" => "sh_features_list",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for some features list section", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the features one per line", SH_NAME)
				),
			)
	    )
);

//Let 's start
vc_map( array(
			"name" => __("Let 's start", SH_NAME),
			"base" => "sh_let_start",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for Let 's start section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("CSS Class", SH_NAME),
				   "param_name" => "cssclass",
				   "description" => __("Enter the CSS Class", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "button_text",
				   "description" => __("Enter the Text for button", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "button_link",
				   "description" => __("Enter the Link for the button", SH_NAME)
				),
			)
	    )
);

/*---------------------home page version2--------------------------*/

//Endless space
vc_map( array(
			"name" => __("Endless Space", SH_NAME),
			"base" => "sh_endless_space",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for Features with endless space section", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for Features with endless space section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the button text for Features with endless space section", SH_NAME)
				),
				
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Class", SH_NAME),
				   "param_name" => "btn_class",
				   "description" => __("Enter the CSS class", SH_NAME)
				),
				
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the button Link for Features with endless space section", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Image", SH_NAME),
				   "param_name" => "img",
				   "description" => __("Choose the section Image.", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Background Image", SH_NAME),
				   "param_name" => "bg_img",
				   "description" => __("Choose the section background Image.", SH_NAME)
				),

			)
	    )
);

//Our Sponsors
vc_map( array(
			"name" => __("Our Sponsors", SH_NAME),
			"base" => "sh_our_sponsors",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for Our Sponsors section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Snippet", SH_NAME),
				   "param_name" => "snippet",
				   "description" => __("Enter the Snippet for Our Sponsors section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of Sponsors to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
			)
	    )
);

//Latest from blog
vc_map( array(
			"name" => __("Latest From Blog", SH_NAME),
			"base" => "sh_latest_from_blog",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for Latest from blog section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number for posts to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);


//Latest from 3 Categories
vc_map( array(
			"name" => __("Latest From 3 Categories", SH_NAME),
			"base" => "sh_latest_from_3_categories",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
                                array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category Two', SH_NAME ),
				   "param_name" => "cat2",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
                                array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category Three', SH_NAME ),
				   "param_name" => "cat3",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

//multi purpose slider
vc_map( array(
			"name" => __("Multi purpose slider", SH_NAME),
			"base" => "sh_multi_purpose",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for Multi purpose slider", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sub title", SH_NAME),
				   "param_name" => "sub_title",
				   "description" => __("Enter the Subtitle for Multi purpose slider", SH_NAME)
				),
				array(
				   "type" => "attach_images",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Images", SH_NAME),
				   "param_name" => "imgs",
				   "description" => __("Choose the images for slider.", SH_NAME)
				),
				
			)
	    )
);

/*-------------Homepage version 3----------------------*/

//Fact buy theme
vc_map( array(
			"name" => __("Fact buy theme", SH_NAME),
			"base" => "sh_fact_buy_theme",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for fact buy theme", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for fact buy theme", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the Text for fact buy theme Button", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the Link for fact buy theme Button", SH_NAME)
				),

			)
	    )
);

//Build your website
vc_map( array(
			"name" => __("Build your website", SH_NAME),
			"base" => "sh_build_your_website",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for the section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of service to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

//Product video
vc_map( array(
			"name" => __("Product Video", SH_NAME),
			"base" => "sh_product_video",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for product video section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sub Title", SH_NAME),
				   "param_name" => "sub_title",
				   "description" => __("Enter the sub title for product video section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Play Link", SH_NAME),
				   "param_name" => "icon_link",
				   "description" => __("Enter the Play video link for section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the Play video button text", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the Button link for the section", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Background Image", SH_NAME),
				   "param_name" => "bg_img",
				   "description" => __("Choose the section background Image.", SH_NAME)
				),

			)
	    )
);

//goodlooking mockup
vc_map( array(
			"name" => __("Goodlooking mockup", SH_NAME),
			"base" => "sh_goodlooking_mockup",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for goodlooking mockup section", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for some features section", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("First Image", SH_NAME),
				   "param_name" => "img1",
				   "description" => __("Choose the section Image.", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Second Image", SH_NAME),
				   "param_name" => "img2",
				   "description" => __("Choose the section Image.", SH_NAME)
				),
			)
	    )
);

//user quicktips
vc_map( array(
			"name" => __("User Quicktips", SH_NAME),
			"base" => "sh_user_quicktips",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for the section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of quicktips to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

/*-------------------------Homepage version 4----------------------------*/

//what we do
vc_map( array(
			"name" => __("What we do", SH_NAME),
			"base" => "sh_what_we_do",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for what we do section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of service to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);


//what we do
vc_map( array(
			"name" => __("Text Box", SH_NAME),
			"base" => "sh_normal_text_box",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title", SH_NAME)
				),
                                array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter Text ", SH_NAME)
				)
			)
	    )
);



//Latest Projects
vc_map( array(
			"name" => __("Latest Projects", SH_NAME),
			"base" => "sh_latest_projects",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for Portfolio section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of portfolio to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			    array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "button_text",
				   "description" => __("Enter the portfolio button text", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "button_link",
				   "description" => __("Enter the portfolio button Link", SH_NAME)
				),
			)
	    )
);

//team
vc_map( array(
			"name" => __("Team", SH_NAME),
			"base" => "sh_team",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for team section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of member to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Department', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Department.', SH_NAME )
				),
				array(
				   "type" => "checkbox",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Show We are Hiring", SH_NAME),
				   "param_name" => "show_hiring",
				   'value' => array('Show We are Hiring' => true ),
				   "description" => __("Whether to show the Hiring area or not", SH_NAME),
				   
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the link of the button", SH_NAME),
				   
				   'dependency' => array(
				   					'element' => 'show_hiring',
									'value' => true
				   				),
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the button text", SH_NAME),
				   
				   'dependency' => array(
				   					'element' => 'show_hiring',
									'value' => true
				   				),
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Hiring Text", SH_NAME),
				   "param_name" => "hiring_text",
				   "description" => __("Enter the Hiring text", SH_NAME),
				   
				   'dependency' => array(
				   					'element' => 'show_hiring',
									'value' => true
				   				),
				),
			)
	    )
);

/*-------------------------Homepage version 5----------------------------*/

//work with us
vc_map( array(
			"name" => __("Work with us", SH_NAME),
			"base" => "sh_work_with_us",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for the section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of service to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);


//single_Portfolio
vc_map( array(
			"name" => __("Single Portfolio", SH_NAME),
			"base" => "sh_single_portfolio",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for Single Portfolio section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of portfolio to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			    
			)
	    )
);

//Blue ribbon
vc_map( array(
			"name" => __("Blue Ribbon", SH_NAME),
			"base" => "sh_blue_ribbon",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for blue ribbon", SH_NAME)
				),
			)
	    )
);


/*---------------------------------portfolio with full width------------------------------- */

//Portfolio full width
vc_map( array(
			"name" => __("Portfolio full width", SH_NAME),
			"base" => "sh_portfolio_full_width",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "desc",
				   "description" => __("Enter Description for Portfolio full width section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of work to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
				
			)
	    )
);

/*---------------------------------portfolio with grid width------------------------------- */

//Portfolio grid width
vc_map( array(
			"name" => __("Portfolio grid width", SH_NAME),
			"base" => "sh_portfolio_grid_width",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "desc",
				   "description" => __("Enter Description for Portfolio grid width section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of work to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
				
			)
	    )
);

/*---------------------------------portfolio with grid 3------------------------------- */

//Portfolio grid three
vc_map( array(
			"name" => __("Portfolio grid 3", SH_NAME),
			"base" => "sh_portfolio_grid_three",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "desc",
				   "description" => __("Enter Description for Portfolio grid 3 section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of work to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
				
			)
	    )
);

/*---------------------------------portfolio with grid 4------------------------------- */

//Portfolio grid four
vc_map( array(
			"name" => __("Portfolio grid 4", SH_NAME),
			"base" => "sh_portfolio_grid_four",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "desc",
				   "description" => __("Enter Description for Portfolio grid 4 section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of work to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
				
			)
	    )
);

/*---------------------------------portfolio with sidebar------------------------------- */

//Portfolio with sidebar
vc_map( array(
			"name" => __("Portfolio with sidebar", SH_NAME),
			"base" => "sh_portfolio_with_sidebar",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "desc",
				   "description" => __("Enter Description for Portfolio grid 4 section", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Column Order", SH_NAME),
				   "param_name" => "column",
				   'value' => array_flip( array('col-2'=>__('col-2', SH_NAME),'col-3'=>__('col-3', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of work to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'portfolio_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
				
			)
	    )
);
/*--------------------------about us page---------------------------------*/
//office
vc_map( array(
			"name" => __("About Office", SH_NAME),
			"base" => "sh_office",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for About office section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("City", SH_NAME),
				   "param_name" => "city",
				   "description" => __("Enter the city where office located", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for about section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the Button Text", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the Button Link", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Image", SH_NAME),
				   "param_name" => "img",
				   "description" => __("Choose the section Image.", SH_NAME)
				),
			)
	    )
);

//blockquote
vc_map( array(
			"name" => __("Blockquotes", SH_NAME),
			"base" => "sh_blockquote",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of blockquote to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'testimonial_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

/*---------------------services---------------------*/

//What we do for you
vc_map( array(
			"name" => __("We do for you", SH_NAME),
			"base" => "sh_we_do_for_you",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for the section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of service to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

//Working Process
vc_map( array(
			"name" => __("Working Process", SH_NAME),
			"base" => "sh_working_process",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter title for Working Process section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of Process to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

/*-----------------shortcode-----------------*/

//button
vc_map( array(
			"name" => __("Button", SH_NAME),
			"base" => "sh_button",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the button text", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the button link", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Style", SH_NAME),
				   "param_name" => "btn_style",
				   'value' => array_flip( array('btn-primary'=>__('btn-primary', SH_NAME),'btn-purple'=>__('btn-purple', SH_NAME),'btn-purple-fill'=>__('btn-purple-fill', SH_NAME),'btn-light-dark'=>__('btn-light-dark', SH_NAME),' '=>__('No-style', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Size", SH_NAME),
				   "param_name" => "btn_size",
				   'value' => array_flip( array('btn-large'=>__('btn-large', SH_NAME),'btn-small'=>__('btn-small', SH_NAME),'btn-mini'=>__('btn-mini', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
			)
	    )
);

//button with icon
vc_map( array(
			"name" => __("Button with icon", SH_NAME),
			"base" => "sh_button_with_icon",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the button text", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the button link", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Style", SH_NAME),
				   "param_name" => "btn_style",
				   'value' => array_flip( array('btn-primary'=>__('btn-primary', SH_NAME),'btn-purple'=>__('btn-purple', SH_NAME),'btn-purple-fill'=>__('btn-purple-fill', SH_NAME),'btn-light-dark'=>__('btn-light-dark', SH_NAME),' '=>__('No-style', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Size", SH_NAME),
				   "param_name" => "btn_size",
				   'value' => array_flip( array('btn-large'=>__('btn-large', SH_NAME),'btn-small'=>__('btn-small', SH_NAME),'btn-mini'=>__('btn-mini', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Icon direction", SH_NAME),
				   "param_name" => "btn_icon",
				   'value' => array_flip( array('left'=>__('Left', SH_NAME),'right'=>__('Right', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
				
			)
	    )
);

//blockquote half
vc_map( array(
			"name" => __("Blockquotes half", SH_NAME),
			"base" => "sh_blockquote_half",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Number", SH_NAME),
				   "param_name" => "num",
				   "description" => __("Enter the number of blockquote to show", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Order By", SH_NAME),
				   "param_name" => "orderby",
				   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Sorting Order", SH_NAME),
				   "param_name" => "order",
				   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
				   "description" => __("Enter the sorting order.", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __( 'Category', SH_NAME ),
				   "param_name" => "cat",
				   "value" => array_flip( sh_get_categories( array( 'taxonomy' => 'testimonial_category', 'hide_empty' => FALSE ) ) ),
				   "description" => __( 'Choose Category.', SH_NAME )
				),
			)
	    )
);

//Fact buy theme
vc_map( array(
			"name" => __("Fact buy theme half", SH_NAME),
			"base" => "sh_fact_buy_theme_half",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title for fact buy theme", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for fact buy theme", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the Text for fact buy theme Button", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the Link for fact buy theme Button", SH_NAME)
				),

			)
	    )
);



//Alert
vc_map( array(
			"name" => __("Alert Box", SH_NAME),
			"base" => "sh_alert",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the title for alert box", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Message", SH_NAME),
				   "param_name" => "msg",
				   "description" => __("Enter the Message for alert", SH_NAME)
				),
				array(
				   "type" => "dropdown",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Alert Style", SH_NAME),
				   "param_name" => "alert_style",
				   'value' => array_flip( array('alert-success'=>__('alert-success', SH_NAME),'alert-info'=>__('alert-info', SH_NAME),'alert-warning'=>__('alert-warning', SH_NAME),'alert-danger'=>__('alert-danger', SH_NAME) ) ),			
				   "description" => __("Enter the column order.", SH_NAME)
				),
				
			)
	    )
);

//Get in touch
vc_map( array(
			"name" => __("Get in touch", SH_NAME),
			"base" => "sh_Get_in_touch",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the title for section", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the Text for section", SH_NAME)
				),
				
			)
	    )
);

//Office Address
vc_map( array(
			"name" => __("Office Address", SH_NAME),
			"base" => "sh_office_address",
			"class" => "",
			"category" => __('craper', SH_NAME),
			"icon" => 'faqs' ,
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the title for section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Address", SH_NAME),
				   "param_name" => "address",
				   "description" => __("Enter the address for section", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Telephone", SH_NAME),
				   "param_name" => "telephone",
				   "description" => __("Enter the telephone number for section", SH_NAME)
				),
                                array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Email", SH_NAME),
				   "param_name" => "email",
				   "description" => __("Enter the email address for section", SH_NAME)
				),
				
			)
	    )
);






/** fun facts with icons */
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("Fun Facts", SH_NAME),
    "base" => "sh_fun_facts",
    "as_parent" => array('only' => 'sh_fact'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
	'icon' => 'fa-smile-o',
	'description' => __('show fun facts', SH_NAME),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for fun facts", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Description", SH_NAME),
            "param_name" => "desc",
            "description" => __("Enter the description for fun facts area", SH_NAME)
        ),
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Fact", SH_NAME),
    "base" => "sh_fact",
    "content_element" => true,
    "as_child" => array('only' => 'sh_fun_facts'), // Use only|except attributes to limit parent (separate multiple values with comma)
	'icon'	=> 'fa-smile-o',
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for fact", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Number", SH_NAME),
            "param_name" => "num",
            "description" => __("Enter the Number for the fact", SH_NAME)
        ),array(
            "type" => "textfield",
            "heading" => __("Link", SH_NAME),
            "param_name" => "link",
            "description" => __("Enter the Link for the fact", SH_NAME)
        ),
		array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon", SH_NAME),
		   "param_name" => "icon",
		   'value' => array_flip(sh_font_awesome()),
		   "description" => __("Choose font awesome icon", SH_NAME)
		),
	   array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon Size", SH_NAME),
		   "param_name" => "icon_size",
		   'value' => array_combine( range( 1, 4, 1 ), range( 1, 4, 1 ) ),
		   "description" => __("Choose icon size", SH_NAME)
		),
    )
) );



//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
class WPBakeryShortCode_Sh_Fun_Facts extends WPBakeryShortCodesContainer {
}
class WPBakeryShortCode_Sh_Fact extends WPBakeryShortCode {
}


/** awesome features */
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("Awesome features list", SH_NAME),
    "base" => "sh_awesome_features_list",
    "as_parent" => array('only' => 'sh_awesome_feature'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
	'icon' => 'fa-smile-o',
	'description' => __('show awesome features list', SH_NAME),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for awesome features", SH_NAME)
        ),
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Awesome Feature", SH_NAME),
    "base" => "sh_awesome_feature",
    "content_element" => true,
    "as_child" => array('only' => 'sh_awesome_features_list'), // Use only|except attributes to limit parent (separate multiple values with comma)
	'icon'	=> 'fa-smile-o',
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for feature", SH_NAME)
        ),
		 array(
            "type" => "textarea",
            "heading" => __("Text", SH_NAME),
            "param_name" => "text",
            "description" => __("Enter the Description text for feature", SH_NAME)
        ),
    )
) );



//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
class WPBakeryShortCode_Sh_Awesome_Features_List extends WPBakeryShortCodesContainer {
}
class WPBakeryShortCode_Sh_Awesome_Feature extends WPBakeryShortCode {
}


/** fun facts both with icons and num */
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("Fun Facts Both", SH_NAME),
    "base" => "sh_fun_facts_both",
    "as_parent" => array('only' => 'sh_fact_both'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
	'icon' => 'fa-smile-o',
	'description' => __('show fun facts both icon and number', SH_NAME),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for fun facts", SH_NAME)
        ),
	),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Fact Both", SH_NAME),
    "base" => "sh_fact_both",
    "content_element" => true,
    "as_child" => array('only' => 'sh_fun_facts_both'), // Use only|except attributes to limit parent (separate multiple values with comma)
	'icon'	=> 'fa-smile-o',
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for fact", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Number", SH_NAME),
            "param_name" => "num",
            "description" => __("Enter the Number for the fact", SH_NAME)
        ),		
		array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon", SH_NAME),
		   "param_name" => "icon",
		   'value' => array_flip(sh_font_awesome()),
		   "description" => __("Choose font awesome icon", SH_NAME)
		),
	   array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon Size", SH_NAME),
		   "param_name" => "icon_size",
		   'value' => array_combine( range( 1, 7, 1 ), range( 1, 7, 1 ) ),
		   "description" => __("Choose icon size", SH_NAME)
		),
    )
) );

/** fun facts both with icons and num with parallex */
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("Fun Facts Both Parallex", SH_NAME),
    "base" => "sh_fun_facts_both_parallex",
    "as_parent" => array('only' => 'sh_fact_both_parallex'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
	'icon' => 'fa-smile-o',
	'description' => __('show fun facts both icon and number with parallex', SH_NAME),
    "params" => array(
        // add params same as with any other content element
		array(
			   "type" => "attach_image",
			   "holder" => "div",
			   "class" => "",
			   "heading" => __("Image", SH_NAME),
			   "param_name" => "img",
			   "description" => __("Choose the section background Image.", SH_NAME)
			),
	),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Fact Both Parallex", SH_NAME),
    "base" => "sh_fact_both_parallex",
    "content_element" => true,
    "as_child" => array('only' => 'sh_fun_facts_both_parallex'), // Use only|except attributes to limit parent (separate multiple values with comma)
	'icon'	=> 'fa-smile-o',
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Button Text", SH_NAME),
            "param_name" => "btn_text",
            "description" => __("Enter the button text for fact", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Button Link", SH_NAME),
            "param_name" => "btn_link",
            "description" => __("Enter the button link for fact", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Number", SH_NAME),
            "param_name" => "num",
            "description" => __("Enter the Number for the fact", SH_NAME)
        ),
		array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon", SH_NAME),
		   "param_name" => "icon",
		   'value' => array_flip(sh_font_awesome()),
		   "description" => __("Choose font awesome icon", SH_NAME)
		),
	   array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon Size", SH_NAME),
		   "param_name" => "icon_size",
		   'value' => array_combine( range( 1, 7, 1 ), range( 1, 7, 1 ) ),
		   "description" => __("Choose icon size", SH_NAME)
		),
    )
) );




//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
class WPBakeryShortCode_Sh_Fun_Facts_Both_Parallex extends WPBakeryShortCodesContainer {
}
class WPBakeryShortCode_Sh_Fact_Both_Parallex extends WPBakeryShortCode {
}

/** Price TAble and price table features */
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("Price Table Container", SH_NAME),
    "base" => "sh_ptable_container",
    "as_parent" => array('only' => 'sh_ptable'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content element
	array(
		   "type" => "dropdown",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Column Order", SH_NAME),
		   "param_name" => "column",
		   'value' => array_flip( array(''=>__('combined', SH_NAME),'col-3'=>__('col-3', SH_NAME),'col-4'=>__('col-4', SH_NAME) ) ),			
		   "description" => __("Enter the column order.", SH_NAME)
		),
	
	),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Price Table", SH_NAME),
    "base" => "sh_ptable",
    "as_parent" => array('only' => 'sh_ptable_features'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "as_child" => array('only' => 'sh_ptable_container'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"content_element" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Title", SH_NAME),
            "param_name" => "title",
            "description" => __("Enter the title for price table area", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Description", SH_NAME),
            "param_name" => "desc",
            "description" => __("Enter the description for price table area", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Price", SH_NAME),
            "param_name" => "price",
            "description" => __("Enter the price with currency symbol", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Button Label", SH_NAME),
            "param_name" => "btn",
            "description" => __("Enter the 'Order Now' button label", SH_NAME)
        ),
		array(
            "type" => "textfield",
            "heading" => __("Button LInk", SH_NAME),
            "param_name" => "btn_link",
            "description" => __("Enter the 'Order Now' button link", SH_NAME)
        ),
		array(
		   "type" => "checkbox",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("is featured?", SH_NAME),
		   "param_name" => "featured",
		   'value' => array('is featured' => true ),
		   "description" => __("Whether to show Is Featured or not?", SH_NAME)
		),
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Price Table Features", SH_NAME),
    "base" => "sh_ptable_features",
    "content_element" => true,
    "as_child" => array('only' => 'sh_ptable'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textarea",
            "heading" => __("Contant", SH_NAME),
            "param_name" => "content",
            "description" => __("Add the pricing table features", SH_NAME),
			'value' => ''
			
        ),
    )
) );



//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
class WPBakeryShortCode_Sh_Ptable_Container extends WPBakeryShortCodesContainer {
}
class WPBakeryShortCode_Sh_Ptable extends WPBakeryShortCodesContainer {
}
class WPBakeryShortCode_Sh_Ptable_Features extends WPBakeryShortCode {
}








/* -------------------------------- Old fields --------------------------- */
/*
*/
/** Social profile with icons */
//Register "container" content element. It will hold all your inner (child) content elements




function sh_custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
//echo $class_string." | ".$tag."<br />";
	if($tag=='vc_row' || $tag=='vc_row_inner') {
		$class_string = str_replace('vc_row-fluid', 'row', $class_string);
	}
	if($tag=='vc_column' || $tag=='vc_column_inner') {
		$class_string = str_replace('vc_span1', 'col-md-1', $class_string);
		$class_string = str_replace('vc_span2', 'col-md-2', $class_string);
		$class_string = str_replace('vc_span3', 'col-md-3', $class_string);
		$class_string = str_replace('vc_span4', 'col-md-4', $class_string);
		$class_string = str_replace('vc_span5', 'col-md-5', $class_string);
		$class_string = str_replace('vc_span6', 'col-md-6', $class_string);
		$class_string = str_replace('vc_span7', 'col-md-7', $class_string);
		$class_string = str_replace('vc_span8', 'col-md-8', $class_string);
		$class_string = str_replace('vc_span9', 'col-md-9', $class_string);
		$class_string = str_replace('vc_span10', 'col-md-10', $class_string);
		$class_string = str_replace('vc_span11', 'col-md-11', $class_string);
		$class_string = str_replace('vc_span12', 'col-md-12', $class_string);
	}
	return $class_string;
}
// Filter to Replace default css class for vc_row shortcode and vc_column
//add_filter('vc_shortcodes_css_class', 'sh_custom_css_classes_for_vc_row_and_vc_column', 10, 2);







function vc_theme_vc_row($atts, $content = null) {
	
   extract(shortcode_atts(array(
		'el_class'        => '',
		'bg_image'        => '',
		'bg_color'        => '',
		'bg_image_repeat' => '',
		'font_color'      => '',
		'padding'         => '',
		'margin_bottom'   => '',
		'container'		  => '',
		'css'			 => ''
	), $atts));
	
	$atts['base'] = '';
	wp_enqueue_style( 'js_composer_front' );
	wp_enqueue_script( 'wpb_composer_front_js' );
	wp_enqueue_style('js_composer_custom_css');
	$vc_row = new WPBakeryShortCode_VC_Row($atts);
	$el_class = $vc_row->getExtraClass($el_class);
	$output = '';
	$css_class =  $el_class;
	
	if( $css ) $css_class .= vc_shortcode_custom_css_class( $css, ' ' ).' ';
	
	$style = $vc_row->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
    
   

   if( $container == 'true' ) {  return '<section '.$style.'><div class="container "><div class="row '.$css_class.'">'.wpb_js_remove_wpautop($content).'</div></div></section>';
   } else {
    return '<section '.$style.'><div class="row'.$css_class.'">'.wpb_js_remove_wpautop($content).'</div></section>';
   }
   
}

function vc_theme_vc_column($atts, $content = null) {
	
   extract( shortcode_atts( array( 'width'=> '1/1', 'el_class'=>'' ), $atts ) );
   
   $width = wpb_translateColumnWidthToSpan($width);
   $width = str_replace('vc_span', 'col-md-', $width);
   $el_class = ($el_class) ? ' '.$el_class : '';
   
   return '<div class="'.$width.$el_class.'">'.do_shortcode($content).'</div>';
}

$param = array(
  "type" => "dropdown",
  "holder" => "div",
  "class" => "",
  "heading" => __("Container", SH_NAME),
  "param_name" => "container",
  "value" => array( 'False'=>'false','True'=>'true'),
  "description" => __("Choose whether you want to add a container before row or not.", SH_NAME)
);

vc_add_param('vc_row', $param);



