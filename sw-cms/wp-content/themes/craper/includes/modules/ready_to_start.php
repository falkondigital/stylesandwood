<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<article class="parallex-section" style="background-image:url(<?php echo wp_get_attachment_url( $img, 'full' ); ?>);">
	<div class="container">
		<div class="align-center">
			<h1 class="animated out" data-animation="fadeInUp" data-delay="0"><?php echo $title;?></h1>
			<h2 class="animated out" data-animation="fadeInUp" data-delay="200"><?php echo $text;?></h2>
			<div class="buttons animated out" data-animation="bounceIn" data-delay="0"> <a href="<?php echo $left_btn_link;?>" class="btn btn-transparent"><?php echo $left_btn_text;?></a> <a href="<?php echo $right_btn_link;?>" class="btn"><?php echo $right_btn_text;?></a> </div>
		</div>
	</div>
</article>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>