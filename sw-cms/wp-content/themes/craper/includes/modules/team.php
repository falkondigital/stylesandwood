<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title; ?></h2>
		</header>
		<div class="teams col-4">
			<ul>

				<?php 
				$curpage = get_the_id();
				$c = 1;
				while( have_posts()): the_post(); 
							$meta = get_post_meta( get_the_id(), '_sh_team_meta', true );//printr($meta);
							
							// added by adi 19 1 2015 
							$customlink = get_post_meta( get_the_id(), 'custom_link', true );
							
							// about page
							if($curpage == 4116){
								$customlink = get_post_meta( get_the_id(), 'custom_link_about', true );
							}

							?>
				<li class="animated out <?php echo 'teamno'.$c; $c++;?>" data-animation="fadeInUp" data-delay="<?php echo $delay;?>">
					<div class="team-box align-center">
						<figure><?php the_post_thumbnail('270x237');?></figure>
						<h5><a href="<?php
							if($customlink != ''){
								echo $customlink;
							}else{
								the_permalink();						
							}
						?>"><?php the_title();?></a></h5>
						<p class="designation"><?php echo sh_set($meta, 'designation');?></p>
						<p><?php echo $this->excerpt(get_the_excerpt(), 100); ?></p>
						<div class="box-btm">
							<ul class="social-links">
								<?php $social_icon = sh_set($meta, 'social_icon_group');

									
									foreach($social_icon as $key => $value):?>
								
										<?php $icon = str_replace( 'fa-', 'icon-', sh_set( $value, 'social_icon' ) ); ?>
										<li><a href="<?php echo sh_set( $value, 'social_url' ); ?>" class="icon-fa <?php echo $icon; ?>" title="<?php echo sh_set( $value, 'title' ); ?>"><span class="icon-fa <?php echo $icon; ?>"></span></a></li>
								
								<?php endforeach; ?>
							
							</ul>
						</div>
						<!-- /box-btm --> 
					</div>
					<!-- /team-box --> 
				</li>
				
				<?php $delay+=200; endwhile; ?>
				<?php if($show_hiring == true):?>
				<li class="animated out" data-animation="fadeInUp" data-delay="<?php echo $delay+200;?>">
					<div class="ad-box align-center">
						<h4>We're hiring</h4>
						<p><?php echo $hiring_text;?></p>
						<a href="<?php echo $btn_link;?>" class="btn btn-purple"><?php echo $btn_text;?></a> </div>
					<!-- /ad-box --> 
				</li>
				<?php endif;?>
			</ul>
		</div>
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>