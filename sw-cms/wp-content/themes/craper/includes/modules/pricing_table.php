<?php ob_start(); ?>

<?php if( $view == 0 ): ?>
<article>
	<div class="section no-padding-bottom">
		<div class="container">
			<div class="pricing-tables <?php echo $column;?>">
				<?php echo do_shortcode( $contents ); ?>
			</div>
		</div>
	</div>
</article>
<?php endif; ?>

<?php if( $view == 1 ): ?>
<div class="pricing-table <?php if($featured == true){echo 'pricing-table-featured';}?>">
	<ul>
		<li class="table-heading"><h5><?php echo $title;?></h5> <span><em><?php echo $price;?></em>/month</span></li>
		
		<?php echo do_shortcode( $contents ); ?>
		
		<li class="table-btm"><a href="<?php echo $btn_link;?>" class="btn btn-theme"><?php echo $btn;?></a></li>
	</ul>
</div>
<?php endif; ?>

<?php if( $view == 2 ): ?>

    <li><?php echo $contents; ?></li>

<?php endif; ?>

<?php $output = ob_get_contents(); 
ob_end_clean();