<?php                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ?><?php

class SH_Taxonomies
{
	function __construct()
	{
		// Hook into the 'init' action
		add_action( 'init', array($this, 'taxonomies'), 0 );
	}
	
	// Register Custom Taxonomy
	function taxonomies()  {
		$labels = array(
			'name'                       => _x( 'Portfolio Category', 'Portfolio Category', SH_NAME ),
			'singular_name'              => _x( 'Portfolio Category', 'Portfolio Category', SH_NAME ),
			'menu_name'                  => __( 'Category', SH_NAME ),
			'all_items'                  => __( 'All Categories', SH_NAME ),
			'parent_item'                => __( 'Parent Category', SH_NAME ),
			'parent_item_colon'          => __( 'Parent Category:', SH_NAME ),
			'new_item_name'              => __( 'New Category Name', SH_NAME ),
			'add_new_item'               => __( 'Add New Category', SH_NAME ),
			'edit_item'                  => __( 'Edit Category', SH_NAME ),
			'update_item'                => __( 'Update Category', SH_NAME ),
			'separate_items_with_commas' => __( 'Separate Categories with commas', SH_NAME ),
			'search_items'               => __( 'Search Categories', SH_NAME ),
			'add_or_remove_items'        => __( 'Add or remove Categories', SH_NAME ),
			'choose_from_most_used'      => __( 'Choose from the most used Categories', SH_NAME ),
		);
	
		$rewrite = array(
			'slug'                       => 'portfolio_category',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
	
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
	
		register_taxonomy( 'portfolio_category', 'sh_portfolio', $args );
		
		/*----------Services category--------------------*/
		
		$labels = array(
			'name'                       => _x( 'Service Category', 'Service Category', SH_NAME ),
			'singular_name'              => _x( 'Service Category', 'Service Category', SH_NAME ),
			'menu_name'                  => __( 'Category', SH_NAME ),
			'all_items'                  => __( 'All Categories', SH_NAME ),
			'parent_item'                => __( 'Parent Category', SH_NAME ),
			'parent_item_colon'          => __( 'Parent Category:', SH_NAME ),
			'new_item_name'              => __( 'New Category Name', SH_NAME ),
			'add_new_item'               => __( 'Add New Category', SH_NAME ),
			'edit_item'                  => __( 'Edit Category', SH_NAME ),
			'update_item'                => __( 'Update Category', SH_NAME ),
			'separate_items_with_commas' => __( 'Separate Categories with commas', SH_NAME ),
			'search_items'               => __( 'Search Categories', SH_NAME ),
			'add_or_remove_items'        => __( 'Add or remove Categories', SH_NAME ),
			'choose_from_most_used'      => __( 'Choose from the most used Categories', SH_NAME ),
		);
	
		$rewrite = array(
			'slug'                       => 'service_category',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
	
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
	
		register_taxonomy( 'service_category', 'sh_service', $args );
		
	/*-------------------testimonial category--------------------*/
		
		$labels = array(
			'name'                       => _x( 'Testimonial Category', 'Testimonial Category', SH_NAME ),
			'singular_name'              => _x( 'Testimonial Category', 'Testimonial Category', SH_NAME ),
			'menu_name'                  => __( 'Category', SH_NAME ),
			'all_items'                  => __( 'All Categories', SH_NAME ),
			'parent_item'                => __( 'Parent Category', SH_NAME ),
			'parent_item_colon'          => __( 'Parent Category:', SH_NAME ),
			'new_item_name'              => __( 'New Category Name', SH_NAME ),
			'add_new_item'               => __( 'Add New Category', SH_NAME ),
			'edit_item'                  => __( 'Edit Category', SH_NAME ),
			'update_item'                => __( 'Update Category', SH_NAME ),
			'separate_items_with_commas' => __( 'Separate Categories with commas', SH_NAME ),
			'search_items'               => __( 'Search Categories', SH_NAME ),
			'add_or_remove_items'        => __( 'Add or remove Categories', SH_NAME ),
			'choose_from_most_used'      => __( 'Choose from the most used Categories', SH_NAME ),
		);
	
		$rewrite = array(
			'slug'                       => 'testimonial_category',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
	
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
	
		register_taxonomy( 'testimonial_category', 'sh_testimonial', $args );
		
			/*-------------------team members category--------------------*/
		
		$labels = array(
			'name'                       => _x( 'Department', 'Department', SH_NAME ),
			'singular_name'              => _x( 'Department', 'Department', SH_NAME ),
			'menu_name'                  => __( 'Department', SH_NAME ),
			'all_items'                  => __( 'All Departments', SH_NAME ),
			'parent_item'                => __( 'Parent Department', SH_NAME ),
			'parent_item_colon'          => __( 'Parent Department:', SH_NAME ),
			'new_item_name'              => __( 'New Department Name', SH_NAME ),
			'add_new_item'               => __( 'Add New Department', SH_NAME ),
			'edit_item'                  => __( 'Edit Department', SH_NAME ),
			'update_item'                => __( 'Update Department', SH_NAME ),
			'separate_items_with_commas' => __( 'Separate Departments with commas', SH_NAME ),
			'search_items'               => __( 'Search Departments', SH_NAME ),
			'add_or_remove_items'        => __( 'Add or remove Departments', SH_NAME ),
			'choose_from_most_used'      => __( 'Choose from the most used Departments', SH_NAME ),
		);
	
		$rewrite = array(
			'slug'                       => 'team_category',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
	
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
	
		register_taxonomy( 'team_category', 'sh_team', $args );

		
				/*----------special deal category--------------------
		
		$labels = array(
			'name'                       => _x( 'Category', 'Special Deal Category', SH_NAME ),
			'singular_name'              => _x( 'Category', 'Category', SH_NAME ),
			'menu_name'                  => __( 'Category', SH_NAME ),
			'all_items'                  => __( 'All Categories', SH_NAME ),
			'parent_item'                => __( 'Parent Category', SH_NAME ),
			'parent_item_colon'          => __( 'Parent Category:', SH_NAME ),
			'new_item_name'              => __( 'New Category Name', SH_NAME ),
			'add_new_item'               => __( 'Add New Category', SH_NAME ),
			'edit_item'                  => __( 'Edit Category', SH_NAME ),
			'update_item'                => __( 'Update Category', SH_NAME ),
			'separate_items_with_commas' => __( 'Separate Categories with commas', SH_NAME ),
			'search_items'               => __( 'Search Categories', SH_NAME ),
			'add_or_remove_items'        => __( 'Add or remove Categories', SH_NAME ),
			'choose_from_most_used'      => __( 'Choose from the most used Categories', SH_NAME ),
		);
	
		$rewrite = array(
			'slug'                       => 'special_deal_category',
			'with_front'                 => true,
			'hierarchical'               => true,
		);
	
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
	
		register_taxonomy( 'special_deal_category', 'bistro_deal', $args );*/
	}
}