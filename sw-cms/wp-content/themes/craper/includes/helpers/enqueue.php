<?php

class SH_Enqueue
{
	var $opt;
	
	function __construct()
	{
		$this->opt = get_option('wp_bistro');
		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );
		
		add_action( 'wp_head', array( $this, 'wp_head' ) );
		
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );
	}

	function sh_enqueue_scripts()
	{
		$styles = array('google_fonts' => 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,700italic,800',
						'layer_slider' =>'css/fullwidth/layerslider.css', 'bootstrap' => 'css/bootstrap.css', 'layer_slider_skin'=>'css/fullwidth/skin.css',
						'prettyPhoto'=>'css/prettyPhoto.css', 'custom_media' => 'css/media.css',
						'flexslider_bistro' => 'css/flexslider.css', 'main_style' => 'style.css');
						
		foreach( $styles as $name => $style )
		{
			if(strstr($style, 'http')) wp_enqueue_style( $name, $style);
			else wp_enqueue_style( $name, SH_URL.$style);
		}
		

		$scripts = array('jquery_transit_modified' => 'jquery-transit-modified.js', 'bootstrap_min' => 'bootstrap.min.js', 'jquery_flexslider_min' => 'jquery.flexslider.min.js',
						'jquery_countdown' => 'countdown.js', 'jquery-raty-min'=>'jquery.raty.min.js', 'jflickrfeed_min' => 'jflickrfeed.min.js', 'jquery_elastislide_min' => 'jquery.elastislide.js', 
						'jquery_imagezoom'=>'jquery.imagezoom.min.js', 'jquery-prettyPhoto'=>'jquery.prettyPhoto.js', 'modernizr_custom_'=>'modernizr.custom.17475.js', 'jquery_transit_modified'=>'jquery-transit-modified.js',	
						'jquery-cookie'=>'jquery.cookie.js','portfolio-filter'=>'portfolio.filter.js', 'jquery_custom'=>'custom.js', 'custom_script' => 'script.js');
						
		foreach( $scripts as $name => $js )
			{
			wp_register_script( $name, SH_URL.'js/'.$js, '', '', true);
		}
		wp_enqueue_script(array('jquery', 'jquery-cookie', 'jquery_transit_modified', 'bootstrap_min', 'jquery_flexslider_min', 'jquery_countdown', 'jflickrfeed_min', 'jquery-raty-min', 
						'modernizr_custom_', 'jquery_transit_modified', 'jquery-prettyPhoto', 'jquery_custom', 'custom_script'));
						
		if( is_singular() ) wp_enqueue_script('comment-reply');
		if( is_page_template( 'portfolio.php' ) ) wp_enqueue_script( 'portfolio-filter' );
		
		if( is_singular( 'wpsc-product' ) ) wp_enqueue_script( array('jquery_elastislide_min', 'jquery_imagezoom') );
	}
	
	function wp_head()
	{
		
		echo '<script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';
		
		//bistro_default_color_scheme();
	}
	
	function wp_footer()
	{
		$analytics = sh_set( $this->opt, 'footer_analytics');
		
		echo $analytics;
	}
}