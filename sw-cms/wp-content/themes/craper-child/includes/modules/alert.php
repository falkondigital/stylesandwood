<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>
<div class="alert <?php echo $alert_style;?>">
	<p><strong><?php echo $title;?></strong> <?php echo $msg;?></p>
</div>
<?php  
$output = ob_get_contents();
ob_end_clean();

?>