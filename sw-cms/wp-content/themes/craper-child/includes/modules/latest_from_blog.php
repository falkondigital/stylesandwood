<?php global $wp_query;
$t = $GLOBALS['_sh_base'];
ob_start();
?>
<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section"> 
	<div class="clients-area">
		<div class="container">
			<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
				<h2><?php echo $title;?></h2>
			</header>
			<ul class="row thumbnails">
				<?php while( have_posts()): the_post();?>
				<?php
					$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );
					//$url = $thumb['0'];
				?>
				<li class="col-xs-12 col-sm-6 col-md-4 animated out" data-animation="bounceIn" data-delay="<?php echo $delay;?>">
					<div class="thumbnail">
						<figure class="image zoom"> <a href="<?php echo $url;?>" data-rel="prettyPhoto" title="<?php echo the_title();?>"><?php the_post_thumbnail('370x300');?></a> </figure>
						<div class="caption">
							<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
							<p><?php echo $this->excerpt(get_the_excerpt(), 200); ?></p>
							<div class="caption-bottom">
								<p class="pull-left"><?php the_time( get_option( 'date_format' ) ); ?></p>
								<ul class="options pull-right">
									<li> <i class="icon-chat-01"></i> <?php comments_number('0', '1', '%' );?> </li>
									<li> <i class="icon-heart"></i> 79 </li>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<?php $delay+=200; endwhile; ?>
			</ul>
		</div>
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>