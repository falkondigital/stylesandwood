<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title;?></h2>
		</header>
		<div class="row">
		
			<?php while( have_posts()): the_post(); 
							$meta = get_post_meta( get_the_id(), '_sh_services_meta', true );//printr($meta);?>
		
			<div class="col-xs-12 col-sm-4 col-md-4 animated out" data-animation="fadeInUp" data-delay="<?php echo $delay;?>">
				<div class="block horizontal-icon">
					<div class="iconic iconic-theme-fill"> <i class="icon-fa <?php echo sh_set($meta, 'font_awesome_icon');?>"></i> </div>
					<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
					
					<ul class="disc-theme">
						<?php $features = sh_set($meta, 'feature_group');

									$delay = 0;
									foreach($features as $key => $value):?>

						<li><?php echo sh_set( $value, 'feature' ); ?></li>
						
						<?php endforeach; ?>
						
					</ul>
					
				</div>
				<!-- /block --> 
			</div>
		
			<?php $delay+=200; endwhile; ?>
		
		</div>
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>