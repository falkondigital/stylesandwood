<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section">
	<div class="container">
		<div class="process-box">
			<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
				<h2><?php echo $title; ?></h2>
			</header>
			<div class="row">
				
				<?php while( have_posts()): the_post(); 
							$meta = get_post_meta( get_the_id(), '_sh_services_meta', true );//printr($meta);?>

			
				<div class="col-xs-12 col-sm-3 col-md-3 process-block animated out" data-animation="fadeInLeft" data-delay="<?php echo $delay;?>">
					<h4><?php the_title();?></h4>
					<div class="iconic iconic-xlarge icon-light-o purple-color"> <i class="icon-fa <?php echo sh_set($meta, 'font_awesome_icon');?> icon-5x"></i> </div>
					<p><?php echo $this->excerpt(get_the_excerpt(), 160); ?></p>
				</div>
				
				<?php $delay+=200; endwhile; ?>
				
			</div>
		</div>
		<!-- /process-box --> 
	</div>
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>