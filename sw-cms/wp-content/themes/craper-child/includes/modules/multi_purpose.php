<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<div class="section no-padding-bottom">
	<div class="container">
		<header class="heading animated out" data-animation="fadeInUp" data-delay="0">
			<h2><?php echo $title;?></h2>
		</header>
		<div class="roundabout-carousel">
			<div class="caro-controls controls-centered"> <a class="caro-prev" href="#"></a> <a class="caro-next" href="#"></a> </div>
			<h4 class="align-center"><?php echo $sub_title;?></h4>
			<ul class="roundabout">
				
				<?php $list = explode(",",$imgs);
					foreach ($list as $value) {
				?>	
				
				<li><?php echo wp_get_attachment_image( $value, 'full' ); ?></li>
				
				
				<?php } ?>
			
			</ul>
		</div>
		<!-- /roundabout-carousel --> 
	</div>
</div>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>