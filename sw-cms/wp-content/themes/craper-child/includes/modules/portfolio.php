<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>


<?php if( have_posts()):?>

<!-- portfolio-area -->
<div class="portfolio-area align-center">
	<header class="heading">
		<h2><?php echo $title;?></h2>
	</header>
	<div class="portfolio-list animated out" data-animation="fadeInUp" data-delay="200">
		<ul>
			
			<?php while( have_posts()): the_post();?>
			<?php
				$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );
				//$url = $thumb['0'];
			?>
			<li>
				<div class="portfolio-box">
					<figure> <a href="<?php echo $url;?>" data-rel="prettyPhoto" title="showcase image"><?php the_post_thumbnail('306x250');?></a>
						<figcaption>
							<h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
							<p><?php echo get_the_term_list(get_the_id(), 'portfolio_category', '', ', '); ?></p>
						</figcaption>
					</figure>
				</div>
			</li>
			<?php endwhile; ?>
		
		</ul>
	</div>
	<a href="<?php echo $button_link;?>" class="btn btn-light-dark btn-icon-hold"><i class="icon-layers"></i><span><?php echo $button_text;?></span></a> </div>
<!-- /portfolio-area --> 

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>