<?php while( have_posts() ): the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( 'blog-box animated out' ); ?> data-animation="fadeInUp" data-delay="0">
		
        <?php $no_image = ( !has_post_thumbnail() ) ? ' no-image' : ''; ?>
        <figure class="image image-hover<?php echo $no_image; ?>"> 
        	
            <?php if( has_post_thumbnail() ): ?>
            	
                <a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail('770x388');?>
                </a>
            
			<?php endif; ?>
            
			<figcaption>
				<h5><?php the_title();?></h5>
				<p><?php the_category(', '); ?></p>
			</figcaption>
		</figure>
		<header class="clearfix">
			<h3 class="pull-left"><a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<ul class="options pull-right">
				<li><a href="<?php the_permalink(); ?>#comments"><i class="icon-chat-01"></i><?php comments_number();?></a></li>
			</ul>
		</header>
		<p class="meta"><?php _e('posted on ', SH_NAME); ?><?php echo get_the_date(); ?> <?php _e('in ', SH_NAME); ?><?php the_category(', '); ?></p>
		<p><?php the_excerpt();?></p>
		<a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>" class="btn btn-small btn-light-dark btn-icon-hold pull-right">
        <i class="icon-page-arrow-right "></i><span><?php _e('Read More', SH_NAME); ?></span>
        </a> 
	</div>

<?php endwhile; ?>
