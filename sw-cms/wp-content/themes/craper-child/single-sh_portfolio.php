<?php  get_header(); ?>
<?php 
$settings = get_post_meta(get_the_ID(), '_sh_portfolio_meta', true);
$sidebar = sh_set( $settings, 'page_sidebar' );
$related = sh_set( $settings, 'is_related_post' );
if(!$sidebar){
	$theme_options = _WSH()->option();
	$sidebar = sh_set( $theme_options, 'page_sidebar' );
	$layout = sh_set( $theme_options, 'blog_layout' );
	$class = ($layout != 'full') ? 'col-md-8' : 'col-md-8';	
	
}else{
	
	$layout = sh_set( $settings, 'layout' );
	$class = ($layout != 'full') ? 'col-md-8' : 'col-md-8';
}

?>

<article class="banner">
	<div class="container">
		
		<h2><?php the_title(); ?></h2>
		
		<div class="breadcrumbs">
			<ul class="breadcrumb">
			<?php //if( get_option( 'show_breadcrumbs') )
				echo get_the_breadcrumb(); ?>
			</ul>
		</div>
		
	</div>
</article>

<?php //if (have_posts()) :?>
	
<article role="main">
	<div class="section">
		<div class="container">
		
		<?php while (have_posts()) : the_post(); 
			
			$meta = get_post_meta( get_the_id(), '_sh_portfolio_meta', true );//printr($meta);?>
		
			<div class="row">
				
				<?php if($layout == 'left'): ?>
        			<div class="col-xs-12 col-sm-4 col-md-4 sidebar"><?php dynamic_sidebar( $sidebar ); ?></div>
        		<?php endif; ?>
				
				<div class="col-xs-12 col-sm-8 <?php echo $class; ?>">
					<div class="contents">
						<div class="detail">
							
							<figure class="image image-hovered"> <?php the_post_thumbnail('770x600');?>
								<figcaption>
									<h5><?php the_title();?></h5>
									<p><?php echo get_the_term_list(get_the_id(), 'portfolio_category', '', ', '); ?></p>
								</figcaption>
							</figure>
							
							<div class="share-bar">
								<div class="addthis_toolbox addthis_default_style">
									<p>Share this post</p>
									<ul>
										<li><a class="addthis_button_tweet"></a></li>
										<li><a id="plusone" class="addthis_button_google_plusone"></a></li>
										<li><a id="facebookLike" class="addthis_button_facebook_like"></a></li>
										<li><a class="addthis_counter addthis_pill_style"></a></li>
									</ul>
								</div>
							</div>
							<!-- /share-bar -->
							<?php if($layout != 'full'){?>
								<?php include( get_template_directory().'/includes/modules/project_description.php');?>
							<?php }?>
 
						</div>
						
						<?php comments_template(); ?>
						
					<div class="clearfix"></div>
						
					<?php if(($related==true)&&($layout != 'full')){ $view = 'with_sidebar';?>
						<?php include( get_template_directory().'/includes/modules/related_posts.php');?>
					<?php }?>
						
					</div>
				</div>
				<?php endwhile;?>
				
				<?php if($layout == 'full'){?>
					 <div class="col-xs-12 col-sm-4 col-md-4 sidebar">
					 	<?php include( get_template_directory().'/includes/modules/project_description.php');?>
					 </div>
				<?php }?>	
				
				<?php if($layout == 'right'): ?>
            		<div class="col-xs-12 col-sm-4 col-md-4 sidebar"><?php dynamic_sidebar( $sidebar ); ?></div>
        		<?php endif; ?>

			</div>
			
			<?php if ( comments_open() ) : ?>
				<!-- If comments are open, but there are no comments. -->
				
				<?php if(($related==true)&&($layout == 'full')){ $view = 'with_comments';?>
					<?php include( get_template_directory().'/includes/modules/related_posts.php');?>
				<?php }?>

	 		<?php else : // comments are closed ?>
				
				<?php if(($related==true)&&($layout == 'full')){ $view = 'simple';?>
					<?php include( get_template_directory().'/includes/modules/related_posts.php');?>
				<?php }?>
	
			<?php endif; ?>
							
		
			<!-- related projects -->
			<div class="related-projects">
				
				<div class="pagination">
					<?php _the_pagination(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
	
<?php get_footer(); 