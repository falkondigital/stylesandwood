<?php get_header(); ?>

<article class="banner">
	<div class="container">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
			<?php //if( get_option( 'show_breadcrumbs') )
				echo get_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</article>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
	<?php the_content(); ?>

<?php endwhile; endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
