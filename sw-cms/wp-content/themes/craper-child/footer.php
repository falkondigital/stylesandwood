 <footer>
        <div class="container">
            <div class="row">
				<?php dynamic_sidebar('footer-sidebar'); ?>
                <?php /*?><div class="col-xs-12 col-sm-6 col-md-3 animated out" data-animation="fadeInUp" data-delay="200">
                    <div class="widget widget_recent_entries">
                        <h4>Get in Touch</h4>
                        <ul class="bullet-2">
                            <li><a href="#">10 Best Design Trends in 2014</a></li>
                            <li><a href="#">How to create a responsive website</a></li>
                            <li><a href="#">Icons made easy with Illustrator</a></li>
                            <li><a href="#">25 Free Iconsets for Webdesigner</a></li>
                            <li><a href="#">How to become a freelancer</a></li>
                        </ul>
                    </div>
                    <!-- /widget --> 
                    
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3  animated out" data-animation="fadeInUp" data-delay="400">
                    <div class="widget widget_tag_cloud">
                        <h4>Tag Cloud</h4>
                        <div class="tagcloud"> <a href="#">Webdesign</a> <a href="#">Freebies</a> <a href="#">Development</a> <a href="#">Script</a> <a href="#">PSD</a> <a href="#">Wordpress Theme</a> </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 animated out" data-animation="fadeInUp" data-delay="600">
                    <div class="widget widget-gallery">
                        <h4>Gallery Widget</h4>
                        <ul>
                            <li><a href="images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"><img src="images/resource/thumb-1.jpg" alt="image"></a></li>
                            <li><a href="images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"><img src="images/resource/thumb-2.jpg" alt="image"></a></li>
                            <li><a href="images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"><img src="images/resource/thumb-3.jpg" alt="image"></a></li>
                            <li><a href="images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"><img src="images/resource/thumb-4.jpg" alt="image"></a></li>
                            <li><a href="images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"><img src="images/resource/thumb-5.jpg" alt="image"></a></li>
                            <li><a href="images/resource/portfolio-1.jpg" data-rel="prettyPhoto" title="showcase image"><img src="images/resource/thumb-6.jpg" alt="image"></a></li>
                        </ul>
                    </div>
                </div>
<?php */?>            </div>
        </div>
    </footer>
</div>
<!-- /pageWrapper -->
<?php wp_footer(); ?>

	<!-- Don't forget analytics -->
	
</body>

</html>
