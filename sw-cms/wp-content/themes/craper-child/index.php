<?php get_header(); 


if( $wp_query->is_posts_page )
{
	
	$quereid_object = get_queried_object();

	$meta = get_post_meta($quereid_object->ID, '_craper_page_meta', true); //printr($meta);exit();
	$sidebar = sh_set( $meta, 'page_sidebar' );
	$layout = sh_set($meta, 'layout');
}else {
	$theme_options = _WSH()->option(); 
	$sidebar = sh_set( $theme_options, 'blog_sidebar');
	$layout = sh_set($theme_options, 'blog_layout');
}


?>

<article class="banner">
	<div class="container">
		<h2><?php _e( 'Blog Archives', SH_NAME ); ?></h2>
		<div class="breadcrumbs">
			<ul class="breadcrumb">
			<?php //if( get_option( 'show_breadcrumbs') )
				echo get_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</article>

<article role="main">
	<div class="section">
		<div class="container">
			<div class="row">
				
				<?php $class = ( $layout == 'full' ) ? 'blog-centered' : 'col-xs-12 col-sm-8 col-md-8';
					if( $layout == 'left' ): ?>
						<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    		<?php dynamic_sidebar( $sidebar ); ?>
                		</div>
            	<?php endif; ?>

				<div class="<?php echo $class; ?>">
					<div class="contents">
							
							<?php get_template_part('content', 'blog'); ?>
							<?php _the_pagination(); ?>
						
					</div>
				</div>
				
				<?php if( $layout == 'right' ): ?>
					
                    <div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    	<?php dynamic_sidebar( $sidebar ); ?>
                	</div>
            	
				<?php endif; ?>

			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
