== WP StrapSlider Lite ==

* By WP Strap Code, http://wpstrapcode.com/

== Copyright ==
WP StrapSlider Lite, Copyright 2013 WPStrapCode.com
WP StrapSlider Lite is distributed under the terms of the GNU GPL


== About WP StrapSlider Lite ==

Requires at least:	3.3.0
Tested up to:		3.5.1
Stable tag:			1.1.2

== License ==
Unless otherwise specified, all the theme files, scripts and images are licensed under GNU General Public Licemse.
The exceptions to this license is as follows:
* Bootstrap by Twitter and the Glyphicon set are licensed under the GPL-compatible [http://www.apache.org/licenses/LICENSE-2.0 Apache License v2.0]

* WP StrapSlider Lite is a Twitter Bootstrap theme for WordPress.
* The theme design includes a home page with the Bootstrap Carousel
* image slider as the focal point in the header - hence the name
* WP StrapSlider. From version 1.0.6 the slider has been further ehnaced
* to give it a more clean look, a new caption look plus a customizer option
* has been added to control the slider effect and even hide it altogether.

* This is the light version with the basics but still an aesthetically
* visual and functional theme.

* It includes support for RTL (Right To Left) layout althought this is
* yet to be fully tested. So for all those that compose content in an
* RTL language I welcome your feedback and support in making this feature
* right for you all - please report any bugs and I'll do my best to fix
* them ASAP. Code contribution is welcome.

== Theme Install And Set Up ==

* The theme can be installed just like anyother theme - either via FTP
* or by uploading it via Admin >> Appearance >> Install Themes

* The slider has been revised to use the posts from the featured category.
* To initiate the slider simply create a new category called "featured" if
* not already implemented or if its new installation. All posts added to 
* this category will cycle through the slider on the home page.
* Visit the customizer page to set your options for the slider - currently
* available options are to set slider to slide or fade transition and to
* hide the slider from the home page altogether. Make sure that your posts
* have a featured image set and at least 1400px wide images.

* By default the slider is set to show 5 latest posts from the category featured.
* This can be changed to any number via the settings options on the customizer. 

* Please note: To initiate the slider effect you must have at least two 
* Posts published within the featured category.

* The rest of the set up is per any other theme - simply post your content.

== Changelog ==

= 1.1.2 =

* Fixed - Bug: The case of the invisible content on comment form text input fields.
* Added option to switch to an laternative navbar with site description for those who need it.
* Added the missing translation strings to smart-widgets.php & strapcode-customizer.php

= 1.1.1 =

* Added permalink on slider title and image as requested on the support forum
* Thread: http://wordpress.org/support/topic/linking-the-slider-images-and-slider-captions-to-the-post-pages
* Added option to set slider excertp length independent of blog feed excerpts.

= 1.1.0 =
* Added option in General/Customizer to switch between full length articles and excerpts
* for blog feeds in response to: 
* http://wordpress.org/support/topic/is-there-a-way-to-limit-the-amount-of-character-shown-on-the-blog-feed
* Added option to define a custom excerpt length to compliment the above change
* Removed redundant functions1.php left over from last update
* Updated to a cleaner Nav_Walker class

= 1.0.9 =
* Moved searchform.php in to the root as it was not being picked up in the sub-folder
* NEW: Added support for cleaner search URL - requires pretty permalinks.
* NEW: Added support to not return the default description in RSS feeds if it has not
* been changed.
* Adjusment made to Navbar to account for an active admin-bar
* NEW: Added Footer credits with options to disable them via the customizer
* NEW: Added a cleaner footer copyright which defaults to Copyright + date + site name
* with option in customizer to add custom copyright
* Removed function wrappers and added correct escaping rules to header.php, footer.php
* and searchform.php
* Removed redundant less folder from the theme.
* Correction to misaligned widgets in the footer
* NEW: Added General option tab to customizer.
* NEW: Added option to hide comment form on single attachment page in the General tab
* of the customizer. More options to come by popular demand and/or on requests.

= 1.0.8 =
* Fixed bug on slider loop which conflicted with the main loop for WordPress versions
* 3.5 and below

= 1.0.7 =
* Removed the remove_version filter from the theme as per ticket #11657
* Removed Custom Post type "Slider"
* Revised slider loop to use posts in the "featured" category for slider
* Added the option to set the number of slides to be shown - default is 5 slides
* Added comments support to static pages
* Removed favicons implementation while I work out the options settings for the
* customizer to allow users to upload own custom favicons

= 1.0.6 =
* Added slider options to theme customizer
* Added option to set slider to slide or fade
* Added option to be able to hide slider on home page
* Removed the loading of jQuery and reverted to native WordPress loaded jQuery to comply with noConflict mode
* Removed if function_exists wrapper from widgets and dynamic_sidebar
* Made the headings color uniform across all theme sections

= 1.0.5 =
* Update Bootstrap and bootstrap-carousel to 2.3.1
* Updated jQuery to 1.9.1
* Cleaned up header code - moved comments enqueu and wp_title to functions.php
* Added the is_home condition to header.php for the carousel
* Moved carousel code to slider.php and removed header-home.php
* Redefined the clear float function to be in line with the themes other functions

= 1.0.4 =
* Code clean up for content
* Content text wrap around thumbnail
* Reworked post meta entries
* Added Genericons iconfonts to post meta entries
* Added language support - theme is now translation ready

= 1.0.3 =
* Moved all styling to style.css
* All stylesheets & scripts are now enqueued rather than being hard coded into templates
* Updated license to GPL3
* Included license.txt file

= 1.0.2 =
1 : Reduced screenshot size as per theme review recommendation 

2 : Changed theme URI to point to the theme demo site as recommended

3 : Added styling for tables and images

4 : Added pagination to both pages and posts 

== Upgrade Notice ==

* PLEASE read before updating to 1.0.7
Version 1.0.7 uses the new WP_Query() for the home page slider - this means that your
current sliders in the custom post type will no longer show up as the custom post
type has been removed from the theme.

As with all updates, please back up your data before updating - and in the case of 
your current sliders please copy these in to new posts and give them the category 
Featured prior to updating. This will ensure that your slider continue to show once 
you update the new version and save you from having to re-do your slider posts from fresh.

This change has been made in order for the theme to fall inline with WordPress best practices
and I personaly offer you my sincere apology for the inconvenience caused.

== Frequently Asked Questions ==

* None Yet