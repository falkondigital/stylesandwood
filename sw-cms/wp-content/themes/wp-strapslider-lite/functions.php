<?php

if (!defined('wpstrapslider_VERSION'))
define('wpstrapslider_VERSION', '1.0');

if ( ! isset( $content_width ) ) $content_width = 940;

/*
| -------------------------------------------------------------------
| Setup Theme
| -------------------------------------------------------------------
|
| */

function wpstrapslider_theme_setup() {
  
 /**
  * Make theme available for translation
  * Translations can be filed in the /languages/ directory
  */
   load_theme_textdomain( 'wpstrapslider', get_template_directory() . '/languages' );
  /**
   * Add editor stylesheet
   */
  //add_editor_style('editor-style.css'); //work in progress
  /**
   * Add default posts and comments RSS feed links to head
   */
  add_theme_support( 'automatic-feed-links' );
  
  /**
   * Adds custom menu with wp_page_menu fallback
   */
  register_nav_menus( array(
	'main-menu' => __( 'Main Menu', 'wpstrapslider' ),
    ) );
	
	/*
| -------------------------------------------------------------------
| Adding Post Thumbnails and Image Sizes
| -------------------------------------------------------------------
| */
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 260, 220 ); // 260 pixels wide by 220 pixels high

  add_image_size( 'wpstrapslider-mini', 160, 120 ); // 160 pixels wide by 120 pixels high
  add_image_size( 'wpstrapslider-small', 260, 180 ); // 260 pixels wide by 180 pixels high
  add_image_size( 'wpstrapslider-medium', 360, 268 ); // 360 pixels wide by 268 pixels high
  add_image_size( 'wpstrapslider-large', 560, 368 ); // 560 pixels wide by 368 pixels high
  add_image_size( 'page-feature', 1230, 440, true ); //(cropped)
  add_image_size( 'thumb-feature', 325, 180, true ); //(cropped)
  add_image_size( 'page-static', 1230, 410, true ); //(cropped)
}
add_action( 'after_setup_theme', 'wpstrapslider_theme_setup' );


################################################################################
// Loading All CSS Stylesheets JS Files
################################################################################
function wpstrapslider_scripts() {
global $wp_styles;
	// CSS Scripts
	wp_enqueue_style( 'wpstrapslider-style', get_stylesheet_uri() );
	wp_enqueue_style('wpstrapslider-bootstrap', get_template_directory_uri().'/assets/css/bootstrap.css', false ,'2.3.1', 'all' );
    wp_enqueue_style('wpstrapslider-responsive', get_template_directory_uri().'/assets/css/bootstrap-responsive.css', false ,'2.3.1', 'all' );
	wp_enqueue_style('wpstrapslider-custom', get_template_directory_uri().'/assets/css/custom.css', false ,'1.0.0', 'all' );
	
	// Load style.css from child theme
    if (is_child_theme()) {
      wp_enqueue_style('strapslider_child', get_stylesheet_uri(), false, null);
    }
	

	// Comments Scripts
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
		
	// JS Scripts
	wp_enqueue_script('bootstrap.min.js', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery'),'2.3.1', true );
	wp_enqueue_script( 'bootstrap-carousel', get_template_directory_uri() . '/assets/js/bootstrap-carousel2.3.1.js', array( 'jquery' ), '2.3.1', true );
	wp_enqueue_script('wpstrapslider-extras', get_template_directory_uri().'/assets/js/wpstrapslider-scripts.js', array('jquery'),'1.0', true );
  }
add_action('wp_enqueue_scripts', 'wpstrapslider_scripts');

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @since WPStrapHero 1.0.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
function wpstrapslider_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'wpstrapslider' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'wpstrapslider_wp_title', 10, 2 );

// Lets load our custom functions and scripts now

require( get_template_directory() . '/assets/inc/smart-widgets.php' );

/**
	 * Custom Nav Menu handler for the Navbar.
	 */
	require_once( get_template_directory() . '/assets/inc/nav-menu-walker.php' );

if ( ! function_exists( 'wpstrapslider_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own wpstrapslider_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function wpstrapslider_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'wpstrapslider' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'wpstrapslider' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite class="fn">%1$s %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span> ' . __( 'Post author', 'wpstrapslider' ) . '</span>' : ''
					);
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'wpstrapslider' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'wpstrapslider' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( __( 'Edit', 'wpstrapslider' ), '<p class="edit-link">', '</p>' ); ?>
			</section><!-- .comment-content -->

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'wpstrapslider' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if ( ! function_exists( 'wpstrapslider_entry_meta' ) ) :
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own wpstrapslider_entry_meta() to override in a child theme.
 *
 * @since WP StrapSlider Lite 1.0.0
 *
 * @return void
 */
function wpstrapslider_entry_meta() {

	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'wpstrapslider' ) . '</span>';

	if ( ! has_post_format( 'aside' ) && ! has_post_format( 'link' ) && 'post' == get_post_type() )
		wpstrapslider_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'wpstrapslider' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'wpstrapslider' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'wpstrapslider' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'wpstrapslider_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own wpstrapslider_entry_date() to override in a child theme.
 *
 * @since WP StrapSlider Lite 1.0.0
 *
 * @param boolean $echo Whether to echo the date. Default true.
 * @return string
 */
function wpstrapslider_entry_date( $echo = true ) {
	$format_prefix = ( has_post_format( 'chat' ) || has_post_format( 'status' ) ) ? _x( '%1$s on %2$s', '1: post format name. 2: date', 'wpstrapslider' ): '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'wpstrapslider' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

/**
 * Display navigation to next/previous pages when applicable
 *
 */
if ( ! function_exists( 'wpstrapslider_content_nav' ) ) :

function wpstrapslider_content_nav() {
	global $wp_query, $wp_rewrite;

	$paged			=	( get_query_var( 'paged' ) ) ? intval( get_query_var( 'paged' ) ) : 1;

	$pagenum_link	=	html_entity_decode( get_pagenum_link() );
	$query_args		=	array();
	$url_parts		=	explode( '?', $pagenum_link );
	
	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}
	$pagenum_link	=	remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link	=	trailingslashit( $pagenum_link ) . '%_%';
	
	$format			=	( $wp_rewrite->using_index_permalinks() AND ! strpos( $pagenum_link, 'index.php' ) ) ? 'index.php/' : '';
	$format			.=	$wp_rewrite->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
	
	$links	=	paginate_links( array(
		'base'		=>	$pagenum_link,
		'format'	=>	$format,
		'total'		=>	$wp_query->max_num_pages,
		'current'	=>	$paged,
		'mid_size'	=>	3,
		'type'		=>	'list',
		'add_args'	=>	array_map( 'urlencode', $query_args )
	) );

	if ( $links ) {
		echo "<nav class=\"pagination pagination-centered clearfix\">{$links}</nav>";
	}
}
endif;

/**
 * Displays page links for paginated posts
 */
if ( ! function_exists( 'wpstrapslider_link_pages' ) ) :

function wpstrapslider_link_pages( $args = array() ) {
	wp_link_pages( array( 'echo' => 0 ));
	$defaults = array(
		'next_or_number'	=> 'number',
		'nextpagelink'		=> __('Next page', 'wpstrapslider'),
		'previouspagelink'	=> __('Previous page', 'wpstrapslider'),
		'pagelink'			=> '%',
		'echo'				=> true
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wpstrapslider_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= '<nav class="pagination clear"><ul><li><span class="dots">' . __('Pages:', 'wpstrapslider') . '</span></li>';
			for ( $i = 1; $i < ($numpages + 1); $i++ ) {
				$j = str_replace( '%', $i, $pagelink );
				if ( ($i != $page) || ((!$more) && ($page!=1)) ) {
					$output .= '<li>' . _wp_link_page($i) . $j . '</a></li>';
				}
				if ($i == $page) {
					$output .= '<li class="current"><span>' . $j . '</span></li>';
				}
				
			}
			$output .= '</ul></nav>';
		} else {
			if ( $more ) {
				$output .= '<nav class="pagination clear"><ul><li><span class="dots">' . __('Pages:', 'wpstrapslider') . '</span></li>';
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= '<li>' . _wp_link_page( $i ) . $previouspagelink. '</a></li>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= '<li>' . _wp_link_page( $i ) . $nextpagelink. '</a></li>';
				}
				$output .= '</ul></nav>';
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}
endif;

/**
 * Sets the post excerpt length to 80 words.
 */
function wpstrapslider_excerpt_length($length) { 
    if ( get_theme_mod('wpstrapslider_excerpt_length') ) : 
       return ( get_theme_mod('wpstrapslider_excerpt_length') ); 
    else : 
       return 80;
    endif;	   
}
add_filter('excerpt_length', 'wpstrapslider_excerpt_length');

// Lets do a separate excerpt length for the slider
function wpstrapslider_slider_excerpt () {
	$theContent = trim(strip_tags(get_the_content()));
		$output = str_replace( '"', '', $theContent);
		$output = str_replace( '\r\n', ' ', $output);
		$output = str_replace( '\n', ' ', $output);
			if (get_theme_mod( 'wpstrapslider_slider_excerpt' )) :
			$limit = get_theme_mod( 'wpstrapslider_slider_excerpt' );
			else : 
			$limit = '40';
			endif;
			$content = explode(' ', $output, $limit);
			array_pop($content);
		$content = implode(" ",$content)."  ";
	return strip_tags($content, ' ');
}

/**
 * Returns a "Read more" link for excerpts
 */
function wpstrapslider_read_more() {
    return '<div class="read-more"><a href="' . get_permalink() . '">' . __('Continue Reading &#8250;&#8250;', 'wpstrapslider') . '</a></div><!-- end of .read-more -->';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and wpstrapslider_read_more_link().
 */
function wpstrapslider_auto_excerpt_more($more) {
    return '<span class="ellipsis">&hellip;</span>' . wpstrapslider_read_more();
}

add_filter('excerpt_more', 'wpstrapslider_auto_excerpt_more');

/**
 * Adds a pretty "Read more" link to custom post excerpts.
 */
function wpstrapslider_custom_excerpt_more($output) {
    if (has_excerpt() && !is_attachment() ) {
        $output .= wpstrapslider_read_more();
    }
    return $output;
}
add_filter('get_the_excerpt', 'wpstrapslider_custom_excerpt_more');

// ADD THEME CUSTOMIZER UNDER APPEARANCE
require( get_template_directory() . '/assets/inc/strapcode-customizer.php' );


add_action('admin_menu', 'add_customizer_to_appearance');
function add_customizer_to_appearance() 
{
  add_theme_page('Customizer', 'Theme Customizer', 'edit_theme_options', 'customize.php', '', '', 6);
}

/* Clearing floats */

function wpstrapslider_clearboth() { ?>
<div class="clearboth"> </div>
<?php } 

/**
 * Redirects search results from /?s=query to /search/query/, converts %20 to +
 *
 * @link http://txfx.net/wordpress-plugins/nice-search/
 */
function wpstrapslider_nice_search_redirect() {
  global $wp_rewrite;
  if (!isset($wp_rewrite) || !is_object($wp_rewrite) || !$wp_rewrite->using_permalinks()) {
    return;
  }
  
  $search_base = $wp_rewrite->search_base;
  if (is_search() && !is_admin() && strpos($_SERVER['REQUEST_URI'], "/{$search_base}/") === false) {
    wp_redirect(home_url("/{$search_base}/" . urlencode(get_query_var('s'))));
    exit();
  }
}

add_action('template_redirect', 'wpstrapslider_nice_search_redirect');

/**
 * Don't return the default description in the RSS feed if it hasn't been changed
 */
function wpstrapslider_remove_default_description($bloginfo) {
  $default_tagline = 'Just another WordPress site';

  return ($bloginfo === $default_tagline) ? '' : $bloginfo;
}

add_filter('get_bloginfo_rss', 'wpstrapslider_remove_default_description');

