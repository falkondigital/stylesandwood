<?php

/*
| -------------------------------------------------------------------
| Registering Widget Sections
| -------------------------------------------------------------------
| */
function wpsmartim_widgets_init() {

  register_sidebar( array(
    'name' => __('Page Sidebar','wpstrapslider'),
    'id' => 'sidebar-page',
	'description'   => __('This sidebar appears on pages only. Accepts shortcode.','wpstrapslider'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h2>',
    'after_title' => '</h2>',
  ) );
  
  register_sidebar( array(
    'name' => __('Blog Sidebar','wpstrapslider'),
    'id' => 'sidebar-blog',
	'description'   => __('This sidebar appears on the right of the home page content. Accepts shortcode.','wpstrapslider'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h2>',
    'after_title' => '</h2>',
  ));

  register_sidebar( array(
    'name' => __('Posts Sidebar','wpstrapslider'),
    'id' => 'sidebar-posts',
	'description'   => __('This sidebar appears on single.php - the individual post page. Accepts shortcode.','wpstrapslider'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h2>',
    'after_title' => '</h2>',
  ));
   
  register_sidebar(array(
    'name' => __('Footer Left - One Third','wpstrapslider'),
    'id'   => 'footer-left',
    'description'   => __('Left Footer Widget - part of three widgets that appear in the footer and are sitewide.','wpstrapslider'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ));

  register_sidebar(array(
    'name' => __('Footer Middle - One Third','wpstrapslider'),
    'id'   => 'footer-middle',
    'description'   => __('Middle Footer Widget - part of three widgets that appear in the footer and are sitewide.','wpstrapslider'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ));

  register_sidebar(array(
    'name' => __('Footer Right - One Third','wpstrapslider'),
    'id'   => 'footer-right',
    'description'   => __('Right Footer Widget - part of three widgets that appear in the footer and are sitewide.','wpstrapslider'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ));
  

}
add_action( 'widgets_init', 'wpsmartim_widgets_init' );
add_filter('widget_text', 'do_shortcode');
add_filter('wp_editor', 'do_shortcode');
add_filter('header_content', 'do_shortcode');