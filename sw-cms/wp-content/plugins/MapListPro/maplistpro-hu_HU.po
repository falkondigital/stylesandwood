msgid ""
msgstr ""
"Project-Id-Version: MapListPro\n"
"POT-Creation-Date: 2013-10-17 21:47-0000\n"
"PO-Revision-Date: 2013-11-19 17:28+0100\n"
"Last-Translator: Csermely Gergely <maplistpro@faxunil.eu>\n"
"Language-Team: SmartRedFox <support@smartredfox.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;_e;__\n"
"X-Poedit-Basepath: .\n"
"Language: English\n"
"X-Poedit-SearchPath-0: .\n"

#: MapListProKO.php:217
msgid "Search..."
msgstr "keresés..."

#: MapListProKO.php:218
msgid "Location..."
msgstr "hely..."

#: MapListProKO.php:240
msgid "Add new map location"
msgstr "Új cím felvétele a térképre"

#: MapListProKO.php:241
msgid "Edit map location"
msgstr "helyszín szerkesztése"

#: MapListProKO.php:242
msgid "New map location"
msgstr "új helyszín"

#: MapListProKO.php:243
msgid "View map location"
msgstr "helyszín megtekintése"

#: MapListProKO.php:244
msgid "Search maps"
msgstr "keresés térképen"

#: MapListProKO.php:462
msgid "Find location"
msgstr "helyszín keresése"

#: MapListProKO.php:463
msgid "Location picker"
msgstr "helyszín jelölő"

#: MapListProKO.php:467
msgid "Latitude"
msgstr "szélesség"

#: MapListProKO.php:471
msgid "Longitude"
msgstr "hosszúság"

#: MapListProKO.php:475
msgid "Short description"
msgstr "rövid ismertető"

#: MapListProKO.php:476
msgid "This appears on the expanded items in the list"
msgstr "Ez jelenik meg a részletes listában"

#: MapListProKO.php:480
msgid "Address"
msgstr "cím"

#: MapListProKO.php:481
msgid "Enter address"
msgstr "Addja meg a címet!"

#: MapListProKO.php:485
msgid "Alternate web address"
msgstr "alternatív weboldal cím"

#: MapListProKO.php:486
msgid "(optional) A full website url (including http://)."
msgstr "opciós: teljes weboldal cím http:// -vel kezdve"

#: MapListProKO.php:521
msgid "Enter a location"
msgstr "Addja meg a helyszínt!"

#: MapListProKO.php:548
msgid "Category icons"
msgstr "kategória ikonok"

#: MapListProKO.php:595
msgid "Simple instructions:"
msgstr "Egyszerű utasítások:"

#: MapListProKO.php:633
msgid ""
"<p>Click to expand and choose a custom icon colour. When there are multiple "
"categories per location, categories at the top of the list show first. Drag "
"and drop to set category order.</p>"
msgstr ""
"<p>Kibontáshoz válassza ki a különböző színű ikonkoat és kattintson rájuk!.  "
"Ha egy helyhez több kategória is tartozik, abban az esetben a legfelső "
"kategória étszik. A kategóriák egérrel fogd és vidd módszerrel "
"átrendezhetőek.</p>"

#: MapListProKO.php:832
msgid "Id"
msgstr "azonosító"

#: MapListProKO.php:844 includes/shortcode_output.php:5
#: includes/shortcode_output.php:116
msgid "Categories"
msgstr "kategóriák"

#: MapListProKO.php:878
msgid "Uncategorized"
msgstr "besorolatlan"

#: MapListProKO.php:1033
msgid "sunday"
msgstr "vasárnap"

#: MapListProKO.php:1036
msgid "monday"
msgstr "hétfő"

#: MapListProKO.php:1039
msgid "tuesday"
msgstr "kedd"

#: MapListProKO.php:1042
msgid "wednesday"
msgstr "szerda"

#: MapListProKO.php:1045
msgid "thursday"
msgstr "csütörtök"

#: MapListProKO.php:1048
msgid "friday"
msgstr "péntek"

#: MapListProKO.php:1051
msgid "saturday"
msgstr "szombat"

#: MapListProKO.php:1359
msgid "location..."
msgstr "helyszín..."

#: MapListProKO.php:1360
msgid "No locations of selected type(s) found."
msgstr "A kiválasztott kategóriához nincs helyszín megadva."

#: MapListProKO.php:1361
msgid "No categories selected."
msgstr "Nincs kiválasztva kategória"

#: MapListProKO.php:1362
msgid "Print directions"
msgstr "Útvonal nyomtatás"

#: MapListProKO.php:1363 includes/shortcode_output.php:99
msgid "No locations found."
msgstr "Nem található helyszín"

#: MapListProKO.php:1364
msgid "Geolocation is not supported by this browser."
msgstr "Ez a böngésző nem támogatja a helymeghatározást."

#: MapListProKO.php:1367
msgid "Kms"
msgstr "km"

#: MapListProKO.php:1368
msgid "Miles"
msgstr "mérföld"

#: MapListProKO.php:1369
msgid "View location"
msgstr "Helyszín megtekintése."

#: MapListProKO.php:1370
msgid "within"
msgstr " "

#: MapListProKO.php:1371 includes/shortcode_output.php:156
msgid "of"
msgstr "-en belül"

#: includes/shortcode_output.php:40 includes/shortcode_output.php:50
msgid "Sort"
msgstr "rendezés"

#: includes/shortcode_output.php:44
msgid "Title"
msgstr "cím"

#: includes/shortcode_output.php:45
msgid "Distance"
msgstr "távolság"

#: includes/shortcode_output.php:72
msgid "Search locations"
msgstr "Helyszín keresés"

#: includes/shortcode_output.php:81 includes/shortcode_output.php:89
#: includes/shortcode_output.php:142
msgid "Go"
msgstr "Menj!"

#: includes/shortcode_output.php:86
msgid "Find locations near"
msgstr "Helyszín keresés a közelben"

#: includes/shortcode_output.php:93
msgid "Clear"
msgstr "törlés"

#: includes/shortcode_output.php:103
msgid "No matching locations"
msgstr "Nincs találat."

#: includes/shortcode_output.php:103
msgid "Show all locations"
msgstr "Minden helyszín megmutatása"

#: includes/shortcode_output.php:135
msgid "View location detail"
msgstr "Helyszín részletes adatai"

#: includes/shortcode_output.php:141
msgid "Get directions from"
msgstr "Útvonal tervezés innen:"

#: includes/shortcode_output.php:143
msgid "Geo locate me"
msgstr "Add meg a pozíciómat!"

#: includes/shortcode_output.php:154
msgid "Next"
msgstr "következő"

#: includes/shortcode_output.php:156
msgid "Page"
msgstr "oldal"

#: includes/shortcode_output.php:158
msgid "Prev"
msgstr "előző"

#: includes/admin/CustomCategoryOrder.php:3
msgid "Custom category icons"
msgstr "egyéb kategória ikonok"

#: includes/admin/CustomCategoryOrder.php:9
msgid "Save options"
msgstr "opciók mentése"

#: includes/admin/SettingsPage.php:35
msgid "Options saved"
msgstr "Opciók elmentve."

#: includes/admin/SettingsPage.php:61
msgid "Settings - Map List Pro"
msgstr "Map List Pro beállításai"

#: includes/admin/SettingsPage.php:66
msgid "Styling"
msgstr "stílus"

#: includes/admin/SettingsPage.php:70
msgid "Pick a style"
msgstr "stílus kiválasztása"

#: includes/admin/SettingsPage.php:97
msgid "Other options"
msgstr "egyéb opciók"

#: includes/admin/SettingsPage.php:98
msgid ""
"These options are site wide and WILL override settings on individual lists."
msgstr "Ezeket a beállításokat a weboldal felülbírálhatja az egyes listákon."

#: includes/admin/SettingsPage.php:103 includes/admin/SettingsPage.php:104
msgid "Distance units"
msgstr "távolság egysége"

#: includes/admin/SettingsPage.php:107
msgid "Imperial"
msgstr "angolszász"

#: includes/admin/SettingsPage.php:108
msgid "Metric"
msgstr "metrikus"

#: includes/admin/SettingsPage.php:114
msgid "Enable full page view for locations"
msgstr "A helyszín teljes oldalas megtekintésének engedélyezése"

#: includes/admin/SettingsPage.php:115
msgid "Enable full page view for locations."
msgstr "A helyszín teljes oldalas megtekintésének engedélyezése"

#: includes/admin/SettingsPage.php:120 includes/admin/SettingsPage.php:121
msgid "Disable styled infoboxes for maps."
msgstr "Formázások tiltás a térkép info boxában"

#: includes/admin/SettingsPage.php:125 includes/admin/SettingsPage.php:126
msgid "Override category label"
msgstr "kategória címkék figyelmen kívül hagyása"

#: includes/admin/SettingsPage.php:133 includes/admin/SettingsPage.php:134
msgid "Google Map Language"
msgstr "Google Map nyelve"

#: includes/admin/SettingsPage.php:138
msgid "ENGLISH"
msgstr "angol"

#: includes/admin/SettingsPage.php:139
msgid "ARABIC"
msgstr "arab"

#: includes/admin/SettingsPage.php:140
msgid "BASQUE"
msgstr "baszk"

#: includes/admin/SettingsPage.php:141
msgid "BULGARIAN"
msgstr "bulgár"

#: includes/admin/SettingsPage.php:142
msgid "BENGALI"
msgstr "bengális"

#: includes/admin/SettingsPage.php:143
msgid "CATALAN"
msgstr "katalán"

#: includes/admin/SettingsPage.php:144
msgid "CZECH"
msgstr "cseh"

#: includes/admin/SettingsPage.php:145
msgid "DANISH"
msgstr "dán"

#: includes/admin/SettingsPage.php:146
msgid "GERMAN"
msgstr "német"

#: includes/admin/SettingsPage.php:147
msgid "GREEK"
msgstr "görög"

#: includes/admin/SettingsPage.php:148
msgid "ENGLISH (AUSTRALIAN)"
msgstr "angol - auszráliai"

#: includes/admin/SettingsPage.php:149
msgid "ENGLISH (GREAT BRITAIN)"
msgstr "angol "

#: includes/admin/SettingsPage.php:150
msgid "SPANISH"
msgstr "spanyol"

#: includes/admin/SettingsPage.php:151
msgid "FARSI"
msgstr "FARSI"

#: includes/admin/SettingsPage.php:152
msgid "FINNISH"
msgstr "finn"

#: includes/admin/SettingsPage.php:153
msgid "FILIPINO"
msgstr "filippínó"

#: includes/admin/SettingsPage.php:154
msgid "FRENCH"
msgstr "francia"

#: includes/admin/SettingsPage.php:155
msgid "GALICIAN"
msgstr "galíciai"

#: includes/admin/SettingsPage.php:156
msgid "GUJARATI"
msgstr "gudzsaráti"

#: includes/admin/SettingsPage.php:157
msgid "HINDI"
msgstr "hindi"

#: includes/admin/SettingsPage.php:158
msgid "CROATIAN"
msgstr "horvát"

#: includes/admin/SettingsPage.php:159
msgid "HUNGARIAN"
msgstr "magyar"

#: includes/admin/SettingsPage.php:160
msgid "INDONESIAN"
msgstr "indonéz"

#: includes/admin/SettingsPage.php:161
msgid "ITALIAN"
msgstr "olasz"

#: includes/admin/SettingsPage.php:162
msgid "HEBREW"
msgstr "héber"

#: includes/admin/SettingsPage.php:163
msgid "JAPANESE"
msgstr "japán"

#: includes/admin/SettingsPage.php:164
msgid "KANNADA"
msgstr "kanadai"

#: includes/admin/SettingsPage.php:165
msgid "KOREAN"
msgstr "koreai"

#: includes/admin/SettingsPage.php:166
msgid "LITHUANIAN"
msgstr "litván"

#: includes/admin/SettingsPage.php:167
msgid "LATVIAN"
msgstr "lett"

#: includes/admin/SettingsPage.php:168
msgid "MALAYALAM"
msgstr "maláj"

#: includes/admin/SettingsPage.php:169
msgid "MARATHI"
msgstr "marathi"

#: includes/admin/SettingsPage.php:170
msgid "DUTCH"
msgstr "holland"

#: includes/admin/SettingsPage.php:171
msgid "NORWEGIAN"
msgstr "norvég"

#: includes/admin/SettingsPage.php:172
msgid "POLISH"
msgstr "lengyel"

#: includes/admin/SettingsPage.php:173
msgid "PORTUGUESE"
msgstr "portugál"

#: includes/admin/SettingsPage.php:174
msgid "PORTUGUESE (BRAZIL)"
msgstr "portugál (brazil)"

#: includes/admin/SettingsPage.php:175
msgid "PORTUGUESE (PORTUGAL)"
msgstr "portugál"

#: includes/admin/SettingsPage.php:176
msgid "ROMANIAN"
msgstr "román"

#: includes/admin/SettingsPage.php:177
msgid "RUSSIAN"
msgstr "orosz"

#: includes/admin/SettingsPage.php:178
msgid "SLOVAK"
msgstr "szlovák"

#: includes/admin/SettingsPage.php:179
msgid "SLOVENIAN"
msgstr "szlovén"

#: includes/admin/SettingsPage.php:180
msgid "SERBIAN"
msgstr "szerb"

#: includes/admin/SettingsPage.php:181
msgid "SWEDISH"
msgstr "svéd"

#: includes/admin/SettingsPage.php:182
msgid "TAGALOG"
msgstr "tagalog"

#: includes/admin/SettingsPage.php:183
msgid "TAMIL"
msgstr "tamil"

#: includes/admin/SettingsPage.php:184
msgid "TELUGU"
msgstr "telugu"

#: includes/admin/SettingsPage.php:185
msgid "THAI"
msgstr "thai"

#: includes/admin/SettingsPage.php:186
msgid "TURKISH"
msgstr "török"

#: includes/admin/SettingsPage.php:187
msgid "UKRAINIAN"
msgstr "ukrán"

#: includes/admin/SettingsPage.php:188
msgid "VIETNAMESE"
msgstr "vietnámi"

#: includes/admin/SettingsPage.php:189
msgid "CHINESE (SIMPLIFIED)"
msgstr "egyszerűsített kínai"

#: includes/admin/SettingsPage.php:190
msgid "CHINESE (TRADITIONAL)"
msgstr "tradícionális kínai"

#: includes/admin/SettingsPage.php:198
msgid "Default location for edit screen"
msgstr "Alapértelmezett hely szerkesztése"

#: includes/admin/SettingsPage.php:200
msgid "Latitude:"
msgstr "szélesség:"

#: includes/admin/SettingsPage.php:203
msgid "Longitude:"
msgstr "hosszúság:"

#: includes/admin/SettingsPage.php:207
msgid "Zoom"
msgstr "nagyítás"

#: includes/admin/SettingsPage.php:208
msgid "Please select"
msgstr "Kérem válasszon!"

#: includes/admin/SettingsPage.php:235
msgid "Save Changes"
msgstr "módosítások mentése"

#: metaboxes/init.php:633 metaboxes/metaboxes/init.php:634
msgid "Please Try Again"
msgstr "Kérem próbálja meg újból!"

#: metaboxes/init.php:650 metaboxes/metaboxes/init.php:651
msgid "Remove Embed"
msgstr "Beágyazás eltávolítása"

#: metaboxes/init.php:656 metaboxes/metaboxes/init.php:657
#, php-format
msgid "No oEmbed Results Found for %s. View more info at"
msgstr "Nincs beágyazott találat %s. További információ:"

#: themefiles/single-maplist.php:45
msgid "Address:"
msgstr "cím:"

#: themefiles/single-maplist.php:58
msgid "Back"
msgstr "vissza"
