��    �      �      	      	  2   	  �   P	     
     
     (
     0
     9
     O
     V
  	   ^
     h
     m
     u
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
      �
  "        6     ?     N     V     k     �  #   �  $   �     �     �     �                         )     =     F     M     S     \  -   j     �     �     �     �     �  	   �  
   �     �     �     �     �          
            
        (  	   1     ;     K  	   W  
   a  	   l     v  (   ~  (   �     �     �  	   �     �     �     �       '   )     Q     g     u     �  
   �     �     �     �     �     �     �     �     �     �            	             '     /     <     I     Z  	   f     p     �     �     �     �     �     �     �     �     �     �  K   �  .   =     l  	   r     |  
   �     �     �     �     �     �  5   �  =     /   O  I     2   �  +   �  )   (  1   R  4   �  5   �  3   �  2   #  D   V  4   �     �     �     �  �   �  =   �  �                  5  	   >     H     `     g     o     w     ~     �     �     �     �     �     �     �  &   �       
     2   $  7   W     �     �     �     �     �     �  5   �  6   0     g     }     �     �  
   �  	   �     �     �  
   �     �                  =      #   ^     �     �     �     �     �     �     �     �  
   �     �     �     �     �     �  	   �       	   
          '  	   6  
   @  	   K     U     ]     d  	   j     t  
   z     �     �  !   �     �  =   �          <     Q     _  	   h     r     �     �     �     �     �     �     �     �     �                         (     F     ^     w     �     �     �     �     �     �                              "  Y   '  <   �     �  	   �     �  
   �     �  "   �           3  
   <     G  $   g  $   �  3   �     �     �  
             '     ?     W     w  *   �     �     �     �     �   (optional) A full website url (including http://). <p>Click to expand and choose a custom icon colour. When there are multiple categories per location, categories at the top of the list show first. Drag and drop to set category order.</p> ARABIC Add new map location Address Address: Alternate web address BASQUE BENGALI BULGARIAN Back CATALAN CHINESE (SIMPLIFIED) CHINESE (TRADITIONAL) CROATIAN CZECH Categories Category icons Clear Custom category icons DANISH DUTCH Default location for edit screen Disable styled infoboxes for maps. Distance Distance units ENGLISH ENGLISH (AUSTRALIAN) ENGLISH (GREAT BRITAIN) Edit map location Enable full page view for locations Enable full page view for locations. Enter a location Enter address FARSI FILIPINO FINNISH FRENCH Find location Find locations near GALICIAN GERMAN GREEK GUJARATI Geo locate me Geolocation is not supported by this browser. Get directions from Go Google Map Language HEBREW HINDI HUNGARIAN INDONESIAN ITALIAN Id Imperial JAPANESE KANNADA KOREAN Kms LATVIAN LITHUANIAN Latitude Latitude: Location picker Location... Longitude Longitude: MALAYALAM MARATHI Map locations general nameMap locations Map locations singular nameMap location Metric Miles NORWEGIAN New map location Next No categories selected. No locations found. No locations of selected type(s) found. No matching locations Options saved Other options POLISH PORTUGUESE PORTUGUESE (BRAZIL) PORTUGUESE (PORTUGAL) Page Pick a style Please select Prev Print directions ROMANIAN RUSSIAN SERBIAN SLOVAK SLOVENIAN SPANISH SWEDISH Save Changes Save options Search locations Search maps Search... Settings - Map List Pro Short description Show all locations Simple instructions: Sort Styling TAGALOG TAMIL TELUGU THAI TURKISH These options are site wide and WILL override settings on individual lists. This appears on the expanded items in the list Title UKRAINIAN Uncategorized VIETNAMESE View location View location detail View map location Zoom location... map_location categoriesAdd New Map Location Category map_location categoriesAdd or remove map location categories map_location categoriesAll Location Categories map_location categoriesChoose from the most used map location categories map_location categoriesEdit Map Location Category map_location categoriesLocation Categories map_location categoriesLocation Category map_location categoriesNew Map Location Category map_location categoriesParent Map Location Category map_location categoriesParent Map Location Category: map_location categoriesPopular Location Categories map_location categoriesSearch Location Categories map_location categoriesSeparate map location categories with commas map_location categoriesUpdate Map Location Category maplist itemAdd new of within Project-Id-Version: Map List Pro v3.2.9
PO-Revision-Date: 2013-07-29 15:56:21+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Generator: CSL v1.x (optionnel) Une addresse URL complète (incluant le http://). <p>Cliquez pour agrandir et choisir une couleur d'icône personnalisée. Lorsqu'il existe plusieurs catégories par emplacement, les catégories en haut de la liste sont affichés en premier. Glissez et déposez pour définir l'ordre des catégories. </p> Arabe Ajouter un nouveau lieux Addresse Addresse: Adresse Web alternative Basque Bengali Bulgare Retour Catalan Chinois (Simplifié) Chinois (Traditionnel) Croate Tchèque Catégories Icônes de catégories Vider Icônes de catégories personnalisées Danois Hollandais L'emplacement par défaut pour l'écran d'édition Désactiver les info-boîtes stylisés pour les cartes. Distance Unités de distance Anglais Anglais (Australien) Anglais (Grande-Bretagne) Modifier l'emplacement Activer l'affichage plein page pour les emplacements. Activer l'affichage pleine page pour les emplacements. Entrer un emplacement Saisissez l'adresse Farsi Pilipino Finlandais Français Trouver un emplacement Trouver des lieux à proximité Galacienne Allemand Grec Gujarati Géolocaliser moi La géolocalisation n'est pas supporté par votre navigateur. Obtenir les directions à partir de Go Langue de Google Map Hébreux Hindi Hongrois Indonésien Italien Id Impériale Japonais Canada Coréen Kms Letton Lituanien Latitude Latitude: Sélecteur de lieu Emplacement... Longitude Longitude: Malayalam Marathi Cartes Lieux Métrique Miles Norvégien Nouvel emplacement Suivante Aucune catégorie sélectionnée. Aucun emplacement trouvé. Aucun emplacement trouvé avec le(s) type(s) sélectionné(s) Aucun emplacement correspondant Options sauvegardés Autre options Polonais Portugais Portugais (Brézil) Portugais (Portugal) Page Choissez un style S'il vous plaît choisir Précécente Imprimer les directions Roumain Russe Serbe Slovaque Slovène Espagnol Suèdois Enregistrer les modifications Sauvegarder les options Recherche d'emplacements Rechercher dans les cartes Recherche... Réglages - Map List Pro Courte description Voir tous les emplacements Instructions simple: Trier Style Tagalog Tamil Telugu Thaï Turc Ces options sont l'échelle du site et annule les réglages sur les listes individuelles. Ceci apparaît sur les éléments développés dans la liste Titre Ukrainien Non catégorisé Vietnamien Voir l'emplacement Voir les détails de l'emplacement Voir l'emplacement Agrandir endroit... Ajouter une nouvelle catégorie Ajouter ou supprimer des catégories Catégories de tous les emplacements Choisissez parmi les catégories les plus utilisés Modifier la catégorie Catégories Catégorie Nouvelle catégorie Parent de la catégorie Parent de la catégorie Catégories de lieux populaires Recherche dans les catégories Séparer les catégories avec des virgules Actualiser la catégorie Ajouter de à l'intérieur 